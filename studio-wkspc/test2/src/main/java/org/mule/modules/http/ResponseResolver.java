package org.mule.modules.http;

import org.apache.log4j.Logger;
import org.mule.api.lifecycle.Initialisable;
import org.mule.api.lifecycle.InitialisationException;
import org.mule.module.http.api.HttpConstants;

import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ResponseResolver is the object that provided error status response details to requests.
 * It is meant to run as a spring singleton within a Mule application.
 */
public class ResponseResolver implements Initialisable {

    private final Logger logger = Logger.getLogger(getClass().getName());
    private static final char COMMENT_CHARACTER = '#';
    private static final String KVP_ASSIGNMENT = "=";

    private String messageMapFile;
    private final Map<Integer, String> messageMap = new ConcurrentHashMap<Integer, String>();

    /**
     * initialise triggers on Mule startup after dependencies have been injected. It looks for the presence of a
     * messageMapFile and if present, will load the responses into memory. These responses are eventually returned from
     * resolveHTTPResponse. If no file is present, status code and response phrase will still function, but no
     * additional message will be returned.
     *
     * @throws InitialisationException
     */
    public void initialise() throws InitialisationException {
        try {
            List<String> lines;
            URI messageMapFileURI = this.getClass().getClassLoader().getResource(messageMapFile).toURI();
            lines = Files.readAllLines(
                    Paths.get(messageMapFileURI), Charset.defaultCharset());

            for (String line : lines) {
                if (line.trim().charAt(0) != COMMENT_CHARACTER) {
                    String[] kvp = line.split(KVP_ASSIGNMENT);
                    messageMap.put(Integer.parseInt(kvp[0]), kvp[1]);
                }
            }
            logger.info("Response messages loaded: " + messageMap.values());
        } catch (Exception err) {
            logger.info("No messageMap.properties file specified. Error response bodies will not include the message file");
        }
    }

    /**
     *
     * HTTPErrorResponse method called by mule to resolve the http status code, response phrase, and message.
     * If now message is present, due to it not being loaded form initialise, the code and response phrase are still
     * returned.
     *
     * @param statusCode is the http status code that a error response should be resolved for
     * @return an object with fields for the http status code, response phrase, and message (to be used in response
     *         body)
     */
    public HTTPErrorResponse resolveHTTPResponse(Integer statusCode) {

        HTTPErrorResponse errorResponse = new HTTPErrorResponse();
        for (HttpConstants.HttpStatus httpStatus : HttpConstants.HttpStatus.values()) {
            if (httpStatus.getStatusCode() == statusCode) {
                errorResponse.setStatusCode(statusCode);
                errorResponse.setResponsePhrase(httpStatus.getReasonPhrase());
            }
            if (messageMap.containsKey(statusCode)) {
                errorResponse.setResponseMessage(messageMap.get(statusCode));
            }
        }
        return errorResponse;
    }

    public void setMessageMapFile(String messageMapFile) {
        this.messageMapFile = messageMapFile;
    }

}
