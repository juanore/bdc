
package cachetemplate;

import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.MuleRuntimeException;
import org.mule.api.processor.MessageProcessor;
import org.mule.construct.Flow;

public class DynamicInvoker implements MessageProcessor {


  @Override
  public MuleEvent process(MuleEvent event) throws MuleException {
    MuleMessage message;

    String payload = null;
    try {
      message = event.getMessage();
      String cacheFillEndpoint = ((DefaultMuleMessage) message).getInvocationProperty("cacheFillEndpoint");
//      payload = event.getMessage().getPayloadAsString();
//      event.getMuleContext().getClient().send(cacheFillEndpoint, message);
      
      Flow flow = (Flow)event.getMuleContext().getRegistry().lookupFlowConstruct(cacheFillEndpoint);   
      event.getMessage().setInvocationProperty("http.method", "GET");
      return flow.process(event);
    } catch (Exception x) {
      throw new MuleRuntimeException(x);
    }
  }

}
