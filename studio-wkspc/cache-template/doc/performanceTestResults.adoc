= Cache Template Performance Results

:sectnums:

== Data

For each run, the following was captured.

* Jmeter configuration
* Mule server's cpu and mem usage
* Mule application's logs
* Jmeter server's cpu and mem usage
* Jmeter result set for each threadgroup

Each test profile has a unique name which maps to a unique directory. Raw data is located in https://drive.google.com/open?id=0B1f_s_g4RwshWEJnZmoxSW8zalk. Consider the following test name.

static-store-contains-300-8000

Represents a test case named `static-store-contains` where `300` users were run at a throughput limit of `8000` transactions per second. This name corresponds with the directory `./static-store-contains-300-8000`.

== Architecture

image:../images/perf-arch.png[width=300]

== Results

=== Test Case: base-line

* 1 thread group
* 600 users
* 10000 tps limit
* 8,400,000 total requests
* Hello world response
* No cache usage

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|8400000
|9
|0
|1136
|23
|0
|9313
|773
|85
|===

image:../images/base-line-tps.png[img,width=550,align=center]

image:../images/base-line-response.png[img,width=550,align=center]

image:../images/base-line-cpu.png[img,width=550,align=center]

image:../images/base-line-mem.png[img,width=550,align=center]

=== Test Case: static-store-contains-read

* 3 thread groups executing in parallel
* Thread group 1: store key x
* Thread group 2: read key x
* Thread group 3: check if cache contains key x

==== 150 Users @ 3000 tps

* 50 users per thread group
* Limit total throughput to 3000tps
* Total of 2,100,000 requests

*Store Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|700000
|1
|0
|276
|2
|0.0
|993
|52
|54

|===

image:../images/store-contains-read-150-300-tps.png[img,width=550,align=center]

image:../images/store-contains-read-150-300-response.png[img,width=550,align=center]

*Read Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|700000
|1
|0
|287
|2
|.05
|993
|193
|199
|===

image:../images/read-contains-read-150-300-tps.png[img,width=550,align=center]

image:../images/read-contains-read-150-300-response.png[img,width=550,align=center]

*Contains Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|700000
|1
|0
|287
|2
|.03
|993
|86
|89

|===

image:../images/contains-contains-read-150-300-tps.png[img,width=550,align=center]

image:../images/contains-contains-read-150-300-response.png[img,width=550,align=center]

*Mule System resources*

image:../images/store-contains-read-150-300-cpu.png[img,width=550,align=center]

image:../images/store-contains-read-150-300-mem.png[img,width=550,align=center]


==== 300 Users @ 8000 tps

* 100 users per thread group
* Limit total throughput to 8000tps
* Total of 4,200,000 requests

*Store Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|1400000
|57
|0
|348
|43
|0
|1634
|86
|54
|===

image:../images/store-contains-read-300-8000-tps.png[img,width=550,align=center]

image:../images/store-contains-read-300-8000-response.png[img,width=550,align=center]

*Read Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|1400000
|37
|0
|312
|31
|0
|1899
|368
|199
|===

image:../images/read-contains-read-300-8000-tps.png[img,width=550,align=center]

image:../images/read-contains-read-300-8000-response.png[img,width=550,align=center]

*Contains Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|1400000
|36
|0
|325
|30
|0
|1896
|165
|89
|===

image:../images/contains-contains-read-300-8000-tps.png[img,width=550,align=center]

image:../images/contains-contains-read-300-8000-response.png[img,width=550,align=center]

*Mule System resources*

image:../images/store-contains-read-300-8000-cpu.png[img,width=550,align=center]

image:../images/store-contains-read-300-8000-mem.png[img,width=550,align=center]

==== 600 Users @ 8000 tps

* 200 users per thread group
* Limit total throughput to 8000tps
* Total of 4,200,000 requests

*Store Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|1400000
|107
|0
|581
|66
|0
|1707
|90
|54
|===

image:../images/store-contains-read-600-8000-tps.png[img,width=550,align=center]

image:../images/store-contains-read-600-8000-response.png[img,width=550,align=center]

*Read Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|1400000
|85
|0
|557
|58
|0
|1806
|350
|199
|===

image:../images/read-contains-read-600-8000-tps.png[img,width=550,align=center]

image:../images/read-contains-read-600-8000-response.png[img,width=550,align=center]

*Contains Thread Group:*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|1400000
|85
|0
|724
|58
|0
|1770
|154
|89
|===

image:../images/contains-contains-read-600-8000-tps.png[img,width=550,align=center]

image:../images/contains-contains-read-600-8000-response.png[img,width=550,align=center]

*Mule System resources*

image:../images/store-contains-read-600-8000-cpu.png[img,width=550,align=center]

image:../images/store-contains-read-600-8000-mem.png[img,width=550,align=center]

=== Test Case: read

* 2 thread groups executing consecutivly
* Thread group 1: store 10 diff keys with 1,200,000 TTL
** Thread group loops 1 time
* Thread group 2: read key based on random number generator
** Uniform distribution (nextInt()) over all available keys

==== 300 Users @ 6000 tps

* 300 users in thread group 2
* Limit total throughput to 6000tps
* Total of 4200000 requests

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|4200000
|27
|0
|294
|24
|0
|5441
|1076
|202
|===

image:../images/read-300-6000-tps.png[img,width=550,align=center]

image:../images/read-300-6000-response.png[img,width=550,align=center]

image:../images/read-300-6000-cpu.png[img,width=550,align=center]

image:../images/read-300-6000-mem.png[img,width=550,align=center]

==== 300 Users @ 5300 tps

* 300 users in thread group 2
* Limit total throughput to 5300tps
* Total of 4200000 requests

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|4200000
|10
|0
|293
|14
|0.0
|5125
|1013
|202
|===

image:../images/read-300-5300-tps.png[img,width=550,align=center]

image:../images/read-300-5300-response.png[img,width=550,align=center]

image:../images/read-300-5300-cpu.png[img,width=550,align=center]

image:../images/read-300-5300-mem.png[img,width=550,align=center]

=== Test Case: contains

* 2 thread groups executing consecutivly
* Thread group 1: store 10 diff keys with 1,200,000 TTL
** Thread group loops 1 time
* Thread group 2: check if OS contains key based on random number generator
** Uniform distribution (nextInt()) over all available keys

==== 300 Users @ 5300 tps

* 300 users in thread group 2
* Limit total throughput to 5300tps
* Total of 4200000 requests

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|4200000
|4
|0
|245
|5
|0
|5240
|455
|89
|===

image:../images/contains-300-5300-tps.png[img,width=550,align=center]

image:../images/contains-300-5300-response.png[img,width=550,align=center]

image:../images/contains-300-5300-cpu.png[img,width=550,align=center]

image:../images/contains-300-5300-mem.png[img,width=550,align=center]

=== Test Case: store-delete

* 2 thread groups executing in parallel
* Thread group 1: stores keys with random number generator spanning 1-250
** Uniform distribution (nextInt()) when generating keys
* Thread group 2: deletes keys with random number generator spanning 1-250
** Uniform distribution (nextInt()) over all available keys

==== 300 Users @ 5300 tps

* 150 users in each thread group
* Limit total throughput to 5300tps
* Total of 4200000 requests

*Store Thread Group*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|2100000
|61
|0
|394
|33
|0
|2062
|109
|54
|===

image:../images/store-store-delete-300-5300-tps.png[img,width=550,align=center]

image:../images/store-store-delete-300-5300-response.png[img,width=550,align=center]

*Delete Thread Group*

[cols="9*", options="header"]
|===
| Samples
| Average
| Min
| Max
| Std Dev
| Error%
| tps
| kb/sec
| avg bytes

|2100000
|51
|0
|408
|36
|0.09
|2096
|204
|100
|===

image:../images/delete-store-delete-300-5300-tps.png[img,width=550,align=center]

NOTE: Exception due to race condition where check for existance returned true, yet when removal fired object was no longer present.

image:../images/delete-store-delete-300-5300-response.png[img,width=550,align=center]

*CPU and Memory*

image:../images/store-delete-300-5300-response-cpu.png[img,width=550,align=center]

image:../images/store-delete-300-5300-response-mem.png[img,width=550,align=center]
