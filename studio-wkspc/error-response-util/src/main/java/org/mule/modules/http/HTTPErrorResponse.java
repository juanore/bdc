package org.mule.modules.http;

public class HTTPErrorResponse {

    private int statusCode;
    private String responsePhrase;
    private String responseMessage;

    public HTTPErrorResponse() {
        this.statusCode = 200;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponsePhrase() {
        return responsePhrase;
    }

    public void setResponsePhrase(String responsePhrase) {
        this.responsePhrase = responsePhrase;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

}
