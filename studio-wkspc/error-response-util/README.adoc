= error-response-util

:sectnums:

This artifact can be included in API-oriented Mule projects and provide consistent error responses across projects. It provides a global error-handling strategy and error response flow that can be referenced from an API application's flow(s).

WARNING: do not use this in any Mule version prior to 3.7.3 (see https://www.mulesoft.org/jira/browse/MULE-8913)

== Behavior

The following describes the expected behavior from a functional perspective.

=== Default behavior

The default behavior of a newly generated APIkit-project is to provide the client back a status code and response message. This is acheived by a global exception strategy that is attached to the main flow. Exceptions must be thrown by a component in the main flow or thrown downstream and bubble up the the main flow to trigger the error-handling logic. Consider the following request and 404 response from the default error-handling configuration.

.Request
[source,txt]
----
curl -i http://localhost:8081/api/badPath
----

.Response
[source,txt]
----
HTTP/1.1 404 
Content-Length: 35
Content-Type: application/json
Date: Fri, 24 Jun 2016 20:36:27 GMT

{ "message": "Resource not found" }
----

=== API error-response-util behavior

Along with being reusable, the error-response-util provides key behavioral changes. error-response-util generates its response body from a template. This allows each response to be unique but also consistent without having to define a `set-payload` for each response. This utility also provides the client with the expected response phrase that correlates to the status code. Lastly, the resuable utility can be called throughout flow logic without the need to explicitly throw an exception and bubble it up to the caller. These items are detailed further in the Setup section. For now, consider the following examples of a 404 and 405 response using error-response-util.

.Request
[source,txt]
----
curl -i http://localhost:8081/api/badPath
----

.Response
[source,txt]
----
HTTP/1.1 404 Not Found
Content-Type: application/json
Content-Length: 66
Date: Fri, 24 Jun 2016 20:51:27 GMT

{
  "error": {
    "code": "404",
    "message": "Resource Not Found"
  }
}
----

NOTE: The corresponding response phrase is attached to the response, in this example, Not Found (after the 404).

.Request
[source,txt]
----
curl -X PATCH http://localhost:8081/api/path
----

.Response
[source,txt]
----
HTTP/1.1 405 Method Not Allowed
Content-Type: application/json
Content-Length: 66
Date: Fri, 24 Jun 2016 20:49:07 GMT

{
  "error": {
    "code": "405",
    "message": "Method Not Allowed"
  }
}
----

NOTE: The message seen in each response body can be modified. This is described further in the Setup section below.

== Setup

=== Artifact build and installation

The following describes the build and installation process of this artifact. If the artifact has already been built and deployed to an link:https://en.wikipedia.org/wiki/Binary_repository_manager[artifact repository] such as Nexus, skip these steps.

. After cloning the repository, `cd` into the error-response-util directory.
+
----
cd $CLONED_REPO_LOCATION/error-response-util
----

. Build and install this artifact to your local repository.
+
----
mvn clean install
----

=== API project configuration

The following describes how to configure and use error-response-util in an API project.

. Create a new, or open an existing, API project.

. Define error-response-util as a dependency.
+
[source%nowrap,xml]
----
<dependency>
  <groupId>com.mulesoft</groupId>
  <artifactId>error-response-util</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
----

. Add the error-response-util.xml to your project via a spring:import.
+
[source%nowrap,xml]
----
<spring:beans>
  <spring:import resource="classpath:error-response-util.xml"/>
</spring:beans>
----

. Remove existing `apikit:mapping-exception-strategy` from the project. 
+
[source%nowrap,xml]
----
<apikit:mapping-exception-strategy name="defaultExceptionStrategy">
  <apikit:mapping statusCode="404">
  <!-- auto generated mapping strategy contents -->  
  </apikit:mapping statusCode="404">
</apikit:mapping-exception-strategy>
----

. Set the inbound HTTP flow's `exception-strategy` to `defaultExceptionStrategy`.
+
[source%nowrap,xml]
----
<flow name="transaction-service-main">
  <http:listener config-ref="httpL" path="/*"/>
  <apikit:router config-ref="api-config"/>
  <exception-strategy ref="defaultExceptionStrategy"/>
</flow>
----
+
NOTE: This strategy catches all `apikit:router` produced exceptions and any exception that bubbles up to this flow. APIkit-based exceptions include those defined in link:https://github.com/mulesoft/apikit/tree/3.8.0/mule-module-apikit/src/main/java/org/mule/module/apikit/exception[org.mule.module.apikit.exception]. These exceptions are caught and handled respective to their appropriate (as defined in link:https://tools.ietf.org/html/rfc7231#section-6.5[RFC7231#6.5]) status code. Other exception types that reach this strategy produce a 500 error response.

. Add a `http:response-builder` to the `http:listner`, which sets the `statusCode` and `reasonPhrase` as seen below.
+
[source%nowrap,xml]
----
<http:listener config-ref="httpL" path="/*">
  <http:response-builder statusCode="#[flowVars['httpStatusCode']]" 
                         reasonPhrase="#[flowVars['httpResponsePhrase']]"/>
</http:listener>
----
+
NOTE: The error-response-util always returns these flow variables for each generated response. Adding them to the `http:response-builder` ensures they are sent back to the client.

. Filter or route all failures, which require client error responses, to `processHTTPErrorResponse`.

.. Example 1: Checking for outbound HTTP failures
+
[source%nowrap,xml]
----
<flow name="get:/some-resource">
  <http:request config-ref="httpR" path="${somePath}" method="${someMethod}">
    <http:success-status-code-validator values="0...599"/>
  </http:request>
  <message-filter onUnaccepted="processHTTPErrorResponse">
    <expression-filter expression="#[message.inboundProperties['http.status'] < 300]"/>
  </message-filter>
  <flow-ref name="transformResponse"/>
</flow>
----
+
NOTE: The above configuration ensures all non-2xx responses are stopped from further processing and sent to have an error response generated based on the returned HTTP status code. This approach prevents the need to throw a costly exception for each non-2xx response. Be aware that this filter stops execution and creates an error response, it will bubble, similar to an exception, the error response to the calling flow.

.. Example 2: Checking empty results
+
[source%nowrap,xml]
----
<sub-flow name="retrieveSingleEntity">
  <db:select config-ref="db-config">
    <db:template-query-ref name="singleEntity.sql"/>
  </db:select>
  <choice doc:name="DB resultList is > 0">
    <when expression="message.payload.size() > 0">
      <flow-ref name="createJSONResponse"/>
    </when>
    <otherwise>
      <set-variable variableName="inboundHTTPStatus" value="404"/>
      <flow-ref name="processHTTPErrorResponse"/>
    </otherwise>
  </choice>
  <flow-ref name="endTransaction"/>
</sub-flow>
----
+
NOTE: The above configuration creates error, 404, responses for all requests that return an empty result set from the database. Unlike the `message:filter` in example 1, after the error response is created it is moved downstream to the next message processor(s). Since the external endpoint is not HTTP-based, setting the variable `inboundHTTPStatus` before requesting an error-response ensures the error-response-util knows what error response to generate.

.. Example 3: Routing exceptions to the response generator
+
[source%nowrap,xml]
----
<flow name="retrieveSingleEntity">
  <db:select config-ref="db-config">
    <db:template-query-ref name="singleEntity.sql"/>
  </db:select>
  <flow-ref name="createJSONResponse"/>
  <flow-ref name="endTransaction"/>
  <catch-exception-strategy>
    <set-variable variableName="inboundHTTPStatus" value="500"/>
    <flow-ref name="processHTTPErrorResponse"/>
  </catch-exception-strategy>
</flow>
----
+
NOTE: The above configuration creates error, 500, responses for all exceptions thrown in this flow. 

. Create a `src/main/resources/messageMap.properties` file containing the message you'd like returned for each status code.
+
[source,txt]
----
404=Resource not found
405=Method used is not acceptable
500=Internal Server Error, an error occurred in the system, please contact your sys-admin
----

.. The above mapping file will aid in creating the response body. If a messageMap is not specified for a returned status code, the message field will be omitted from the response. Consider the 415 response below. 
+
[source,txt]
----
curl -X POST -i http://localhost:8081/test
HTTP/1.1 415 Unsupported Media Type
Content-Length: 48
Date: Fri, 24 Jun 2016 19:22:18 GMT
----
+
[source,json]
----
{
  "error": {
    "code": "415"
  }
}
----

.. If a messageMap is found, the message field will be included. Consider the 404 response below.
+
[source,txt]
----
curl -i http://localhost:8081/not/real/resource
HTTP/1.1 404 Not Found
Content-Length: 66
Date: Fri, 24 Jun 2016 19:22:18 GMT
----
+
[source,json]
----
{
  "error": {
    "code": "404",
    "message": "Resource not found"
  }
}
----

=== Optional: error-response-util customization

The following describes how the error-response-util can be customized to meet your desired behavior. Once the utility is setup to meet your requirements, package and deploy it to be shared amongst your team.

==== Response body

Each error response body is generated from the template `src/main/resources/errorResponse.json`. Customize this template as needed and note that it supports MEL.

==== Custom error routes

By default, error-response-util uses the status code and response phrase found in link:https://github.com/mulesoft/mule/blob/mule-3.8.0/modules/http/src/main/java/org/mule/module/http/api/HttpConstants.java#L72-L115[org.mule.module.http.api.HttpConstants] to resolve what should be returned to the client. If one or many error statuses must have a customized response phrase, status code, and/or response body, the `processHTTPErrorResponse` flow can be modified.

In `processHTTPErrorResponse`, you'll find the following choice router capable of sending a error status to a specific sub-flow.

[source%nowrap,xml]
----
<sub-flow name="processHTTPErrorResponse" doc:description="processHTTPErrorResponse">
  <expression-component doc:name="Extract HTTP status to flowVars['inboundHTTPStatus']">
    <![CDATA[
    if (flowVars['inboundHTTPStatus'] == empty) {
      if (message.inboundProperties['http.status'] == empty) {
          flowVars['inboundHTTPStatus'] = 200;
      } else {
          flowVars['inboundHTTPStatus'] = message.inboundProperties['http.status'];
      };
    };
    ]]>
  </expression-component>
  <choice doc:name="Route response based on flowVars['inboundHTTPStatus']">
    <when expression="#[flowVars['httpInboundStatus'] == 407]">
      <flow-ref name="create404Response" doc:name="create404Response"/>
    </when>
    <otherwise>
      <flow-ref name="createResponse" doc:name="createResponse"/>
    </otherwise>
  </choice>
  <parse-template location="errorResponse.json" doc:name="Create error response body"/>
</sub-flow>
----

WARNING: 407 is only used here as an example. It may be removed in your real implementaton.

All entering error codes, except 407, will route route to `createResponse`, which generates the status code and reason phrase based on link:https://github.com/mulesoft/mule/blob/mule-3.8.0/modules/http/src/main/java/org/mule/module/http/api/HttpConstants.java#L72-L115[org.mule.module.http.api.HttpConstants], as described above. If a 407 were to enter, say from a backend response, it would be routed to `create404Response`, a sub-flow designed for specific handling of 407's to map them into 404s.

== Implementation reference

The following implementation details are documented, as comments, in the mule-config xml. They have been copied here for convienence.

=== responseResolver bean

responseResolver resolves http response phrases and messages (to be included in a response body). A single instance of this thread-safe bean provides response data to all callers.

[source%nowrap,xml]
----
<spring:beans>
  <spring:bean name="responseResolver" id="responseResolver" scope="singleton" class="org.mule.modules.http.ResponseResolver">
    <spring:property name="messageMapFile" value="messageMap.properties"/>
  </spring:bean>
</spring:beans>
----

=== processHTTPErrorResponse sub-flow

processHTTPErrorResponse routes an error to the appropriate response creator. After receiving response details from a response creator, processHTTPErrorResponse generates a response body based on the provided template in errorResponse.json. This flow is the primary entry point for all error response calls. Error response calls will, by default, be routed to createResponse, which uses responseResolver to provide consistent error response details. If a highly customized error response is desirable for a specific status code, a separate flow can be referenced for the choice which routes the error code to a specialized flow to set the status code, response phrase, and message.  

==== Inputs

processHTTPErrorResponse expects a http status code as an Integer (or int) type.  The status code can be passed as flowVars['inboundHTTPStatus'] or message.inboundProperties['http.status'].  The former is most common if error statuses are not coming from an external http requests. Such as the need to map an error status to a caught exception. The latter is most common when filtering http error responses to processHTTPErrorResponse. If a status code is not passed through either means, it is assumed the status code is 200.  

==== Returns

processHTTPErrorResponse returns a http response: status code, response phrase (https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html), and a response body.
                                                                                                          
* `flowVars['httpStatusCode']`: HTTP status code to return to client. Value is typically passed as
                                <http:response-builder statusCode="#[flowVars['httpStatusCode']]"/>
                                                                                                          
* `flowVars['httpResponsePhrase']`: HTTP response phrase to return to client. Value is typically passed as
                                    <http:response-builder reasonPhrase="#[flowVars['httpResposnePhrase']]"/>
                
* `payload`: HTTP response body, generated from template, to return to client. Returned as payload, unless otherwise modified, will be returned to client as response body.

[source%nowrap,xml]
----
<sub-flow name="processHTTPErrorResponse" doc:description="processHTTPErrorResponse">
	<expression-component doc:name="Extract HTTP status to flowVars['inboundHTTPStatus']">
		<![CDATA[
		if (flowVars['inboundHTTPStatus'] == empty) {
			if (message.inboundProperties['http.status'] == empty) {
					flowVars['inboundHTTPStatus'] = 200;
			} else {
					flowVars['inboundHTTPStatus'] = message.inboundProperties['http.status'];
			};
		};
		]]>
	</expression-component>
	<choice doc:name="Route response based on flowVars['inboundHTTPStatus']">
		<when expression="#[flowVars['httpInboundStatus'] == 407]">
			<flow-ref name="create404Response" doc:name="create404Response"/>
		</when>
		<otherwise>
			<flow-ref name="createResponse" doc:name="createResponse"/>
		</otherwise>
	</choice>
	<parse-template location="errorResponse.json" doc:name="Create error response body"/>
</sub-flow>
----

=== createResponse sub-flow 

createResponse invokes the responseResolver bean to retrieve errorResponse data. The response data contains status code, response phrase, and response message (used in response body). The response phrase returned from this flow is obtained from Mule's internal list of status code/response phrases. Each piece of response data is returned in its own flow variable. This flow is called from processHTTPErrorResponse.

==== Inputs

createResponse expects flowVars['httpInboundStatus'] containing a Integer or int.

==== Returns

createResponse returns a variable for each piece of resposne data.  

* flowVars['httpStatusCode']: HTTP status code to be used in the response.

* flowVars['httpResponsePhrase']: HTTP response phrase, matching the status code, to be used in the response.

* flowVars['httpResponseMessage']: Message reflecting the error to be included in the final response body.

[source%nowrap,xml]
----
<sub-flow name="createResponse">
	<enricher doc:name="Message Enricher">
		<invoke object-ref="responseResolver"
						method="resolveHTTPResponse"
						methodArguments="#[flowVars['inboundHTTPStatus']]"
						doc:name="call resolveHTTPResponse()"/>
		<enrich source="#[payload.statusCode]" target="#[flowVars['httpStatusCode']]"/>
		<enrich source="#[payload.responsePhrase]" target="#[flowVars['httpResponsePhrase']]"/>
		<enrich source="#[payload.responseMessage]" target="#[flowVars['httpResponseMessage']]"/>
	</enricher>
</sub-flow>
----

=== defaultExceptionStrategy exception strategy

defaultExceptionStrategy catches all APIkit-based exception and has a catch all for other exception types. It's often referenced from the main inbound flow, containing the APIkit router, of the API.  Exceptions caught in this strategy are assigned a status code in flowVars['inboundHTTPStatus'] and are forwarded on to processHTTPErrorResponse to generate a response.

==== Returns

defaultExceptionStrategy returns the response from processHTTPErrorResponse

[source%nowrap,xml]
----
<apikit:mapping-exception-strategy name="defaultExceptionStrategy">
	<apikit:mapping statusCode="404">
		<apikit:exception value="org.mule.module.apikit.exception.NotFoundException"/>
		<set-variable variableName="inboundHTTPStatus" value="404" doc:name="inboundHTTPStatus: 404"/>
		<flow-ref name="processHTTPErrorResponse" doc:name="processHTTPErrorResponse"/>
	</apikit:mapping>
	<apikit:mapping statusCode="405">
		<apikit:exception value="org.mule.module.apikit.exception.MethodNotAllowedException"/>
		<set-variable variableName="inboundHTTPStatus" value="405" doc:name="inboundHTTPStatus: 405"/>
		<flow-ref name="processHTTPErrorResponse" doc:name="processHTTPErrorResponse"/>
	</apikit:mapping>
	<apikit:mapping statusCode="415">
		<apikit:exception value="org.mule.module.apikit.exception.UnsupportedMediaTypeException"/>
		<set-variable variableName="inboundHTTPStatus" value="415" doc:name="inboundHTTPStatus: 415"/>
		<flow-ref name="processHTTPErrorResponse" doc:name="processHTTPErrorResponse"/>
	</apikit:mapping>
	<apikit:mapping statusCode="406">
		<apikit:exception value="org.mule.module.apikit.exception.NotAcceptableException"/>
		<set-variable variableName="inboundHTTPStatus" value="406" doc:name="inboundHTTPStatus: 406"/>
		<flow-ref name="processHTTPErrorResponse" doc:name="processHTTPErrorResponse"/>
	</apikit:mapping>
	<apikit:mapping statusCode="400">
		<apikit:exception value="org.mule.module.apikit.exception.BadRequestException"/>
		<set-variable variableName="inboundHTTPStatus" value="400" doc:name="inboundHTTPStatus: 400"/>
		<flow-ref name="processHTTPErrorResponse" doc:name="processHTTPErrorResponse"/>
	</apikit:mapping>
	<!-- catch all strategy -->
	<apikit:mapping statusCode="500">
		<apikit:exception value="java.lang.Exception"/>
		<set-variable variableName="inboundHTTPStatus" value="500" doc:name="inboundHTTPStatus: 500"/>
		<flow-ref name="processHTTPErrorResponse" doc:name="processHTTPErrorResponse"/>
	</apikit:mapping>
</apikit:mapping-exception-strategy>
----
