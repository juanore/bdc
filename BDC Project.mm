<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="BDC Project" FOLDED="false" ID="ID_100199644" CREATED="1499173316804" MODIFIED="1499173330909" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Scope" FOLDED="true" POSITION="right" ID="ID_394890687" CREATED="1499173345937" MODIFIED="1499173349211">
<edge COLOR="#ff0000"/>
<node TEXT="BDC Internal Prep&#xa;Scope of the engagement is as follows:&#xa;Discovery and Architecture Blueprint&#xa;&#xb7;    Understand current state integration architecture and map to target landscape architecture leveraging Mule platform&#xa;&#xb7;    Define initial architecture, use cases design, and data flows for the identified use cases&#xa;&#xb7;    Define security patterns for common authentication and authorization&#xa;&#xb7;    Define common logging and exception handling patterns based on best practices&#xa;&#xb7;    Design High-level Mule flows and mock application architecture as reference implementation for identified use cases&#xa;&#xb7;    Assist with provisioning up to 3 environments for Mule platform and (Dev, Pre-Prod and Prod)&#xa;&#xa;Use Case Implementation&#xa;&#xb7;    Assist in implementing and unit testing up to seventeen (17) use cases&#xa;&#xb7;    Configure federation of ARM access management to a SAML2.0 identity provider such as Microsoft ADFS or F5 APM&#xa;&#xb7;    Configure ARM to map the internal BDC roles to an ARM role for role-base access control&#xa;&#xb7;    Leveraging the MuleSoft connector SDK, configure a reusable asset that supports Kerberos SSO to enable role-based access control. For instance, enable web service consumer for SOAP component to:&#xa;&#xa7;  Modify or forward Kerberos ticket to back-end application&#xa;&#xa7;  Create or use a Kerberos ticket to access back-end server&#xa;&#xb7;    Create a Kerberos ticket to appropriately impersonate a user to access the back-end application&#xa;&#xb7;    Configure MuleSoft as Oauth2 authorization server with the ability to generate authorization grant types, including a JWT format.&#xa;&#xb7;    Leverage MuleSoft as a SAML 2.0 validator&#xa;&#xb7;    Configure MuleSoft to support valid Oauth2 access token, with appropriate authorizations for API access, including a JWT format.&#xa;&#xb7;    Help establish a framework reusable test cases based on initial use cases using MUnit&#xa;&#xb7;    Help implement common services around logging with a logging service to be determined, alerting, error handling, security and monitoring (app dynamics) and integrate these within the environment&#xa;&#xb7;    Assist with deploying MuleSoft applications&#xa;&#xa;&#xa;Deployment and Transition Support&#xa;&#xb7;    Assist with testing support, and defect fixes&#xa;&#xb7;    Assist with deploying MuleSoft applications/proxies with policies to DEV/UAT environments&#xa;&#xb7;    Provide guidance on the integration testing, allowing the customer team to be actively involved in to help ensure skills transfer is completed&#xa;&#xb7;    Enable Customer&#x2019;s resources through development shadowing" ID="ID_1376653483" CREATED="1499175941933" MODIFIED="1499175947854"/>
</node>
<node TEXT="license" FOLDED="true" POSITION="right" ID="ID_985225894" CREATED="1499174673782" MODIFIED="1499174677000">
<edge COLOR="#ff00ff"/>
<node TEXT="current" ID="ID_1435433877" CREATED="1499174678694" MODIFIED="1499174681839">
<node TEXT="" ID="ID_1551434826" CREATED="1499174683022" MODIFIED="1499174711098">
<hook URI="License/Screen%20Shot%202017-06-28%20at%2012.45.48%20PM.png" SIZE="0.21613833" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Week1" FOLDED="true" POSITION="right" ID="ID_1137617986" CREATED="1499254872066" MODIFIED="1499254875197">
<edge COLOR="#7c0000"/>
<node TEXT="agenda" ID="ID_808913544" CREATED="1499254877810" MODIFIED="1499254880819">
<node TEXT="" ID="ID_1042149287" CREATED="1499254884882" MODIFIED="1499254917233" LINK="meetings/Mulesoft%20Week%201%20Schedule.xlsx"/>
</node>
</node>
<node TEXT="Project" POSITION="right" ID="ID_1488194480" CREATED="1499697449319" MODIFIED="1499697454663">
<edge COLOR="#00007c"/>
<node TEXT="current" ID="ID_483758555" CREATED="1499697480658" MODIFIED="1499699010983">
<cloud COLOR="#ffccff" SHAPE="ROUND_RECT"/>
<font BOLD="true"/>
<node ID="ID_1631916275" CREATED="1499697485235" MODIFIED="1499697636508"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        BDC has build microservices in top of their current systems.
      </li>
      <li>
        Existing Microservices are SOAP webservices that deals with Kerberos.
      </li>
      <li>
        All Microervices are expose thru Systinet.
      </li>
    </ol>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Future" ID="ID_1822633835" CREATED="1499697662419" MODIFIED="1499699003745">
<font BOLD="true"/>
<node ID="ID_94792898" CREATED="1499697668963" MODIFIED="1499698872728"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        There are two channels that will consume the existing microservices.
      </li>
      <li>
        Mobile and Web Channels.

        <ol>
          <li>
            Mobile are use by BDC employees.
          </li>
          <li>
            Web will be use by third parties.
          </li>
        </ol>
      </li>
      <li>
        build 17 Orchestrators (Now called Process APIs)

        <ol>
          <li>
            9 Process APIs are build and deployed into Systinet (July-2017)
          </li>
          <li>
            8 Process API (no documentation) to de in production by end of Jan 2018.
          </li>
          <li>
            Process API can call other Process
          </li>
        </ol>
      </li>
      <li>
        build 17 experience APIs for Mobile
      </li>
      <li>
        build 17 experience APIs for Web
      </li>
      <li>
        build 60 system APIs (one per microservice=wsdl) and replace Systinet.

        <ol>
          <li>
            aprox 33 microservice are used by the Process APIs.
          </li>
          <li>
            System call other system using microsevices thru Systinet.
          </li>
        </ol>
      </li>
    </ol>
  </body>
</html>

</richcontent>
<cloud COLOR="#ccffcc" SHAPE="ROUND_RECT"/>
</node>
</node>
<node TEXT="RISK" ID="ID_1076121997" CREATED="1499698246127" MODIFIED="1499698996501">
<cloud COLOR="#ffcc99" SHAPE="RECT"/>
<font BOLD="true"/>
<node ID="ID_1099844380" CREATED="1499698253935" MODIFIED="1499699237499"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        BDC found a security leak during POC.
      </li>
      <li>
        The security architecture has changed in order to support the web channel.

        <ol>
          <li>
            The security architect wants to discuss the security architecture with the Mulesoft Security expert. He has many concerns.
          </li>
        </ol>
      </li>
      <li>
        The system API &quot;must continue&quot; using soap/ws since it is called internally by other systems.
      </li>
      <li>
        Process API may continue handle xml, BDC concerns about performance impact in translating to JSON
      </li>
      <li>
        During POC, Mule could import WSDL from CLS
      </li>
      <li>
        Some Microservices contains documents in the payload.
      </li>
    </ol>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="contacts" POSITION="left" ID="ID_1958699980" CREATED="1499173352465" MODIFIED="1499173356347">
<edge COLOR="#0000ff"/>
<node TEXT="BDC" ID="ID_1247019403" CREATED="1499173357905" MODIFIED="1499173360531">
<node TEXT="phil.prud&apos;homme@bdc.ca" ID="ID_1233188634" CREATED="1499174346134" MODIFIED="1499695238020"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      project manager
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Haroun Bouazzi" FOLDED="true" ID="ID_1774154841" CREATED="1499696129587" MODIFIED="1499696256018">
<icon BUILTIN="bookmark"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Enterprise Architect
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Haroun.BOUAZZI@bdc.ca" ID="ID_1005469789" CREATED="1499696160401" MODIFIED="1499696165882"/>
</node>
<node TEXT="Benoit Vinola" FOLDED="true" ID="ID_276457640" CREATED="1499695806603" MODIFIED="1499696263205">
<icon BUILTIN="bookmark"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Solution Architect
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Benoit.VINOLAS@bdc.ca" ID="ID_1679716437" CREATED="1499695816003" MODIFIED="1499695818340"/>
</node>
<node TEXT="Guillaume Nourry-Marquis" FOLDED="true" ID="ID_1744557799" CREATED="1499695600323" MODIFIED="1499696266881">
<icon BUILTIN="bookmark"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Security Architect
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Guillaume.NOURRY-MARQUIS@bdc.ca" ID="ID_1758434914" CREATED="1499695631860" MODIFIED="1499695633829"/>
</node>
<node TEXT="Brian Meredith" FOLDED="true" ID="ID_1192794614" CREATED="1499695673109" MODIFIED="1499695706450"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Solution Architect - API Manager
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Brian.MEREDITH@bdc.ca" ID="ID_999499972" CREATED="1499695681989" MODIFIED="1499695685311"/>
</node>
<node TEXT="Tim Lanh Huynh Huu" ID="ID_53489082" CREATED="1499695872384" MODIFIED="1499695906745"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Mulesoft Developer
    </p>
  </body>
</html>

</richcontent>
<node TEXT="TimLanh.HUYNHHUU@bdc.ca" ID="ID_1211458717" CREATED="1499695893150" MODIFIED="1499695895367"/>
</node>
<node TEXT="Ousmane Mbacke" FOLDED="true" ID="ID_1971910814" CREATED="1499695924455" MODIFIED="1499695980599"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Leader Web Development
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Ousmane.MBACKE@bdc.ca" ID="ID_1346000567" CREATED="1499695941951" MODIFIED="1499695943744"/>
</node>
<node TEXT="Aaron Burgermesiter" FOLDED="true" ID="ID_539858167" CREATED="1499696000064" MODIFIED="1499696041293"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Infrastructure
    </p>
  </body>
</html>

</richcontent>
<node TEXT="AARON.BURGERMEISTER@bdc.ca" ID="ID_1657985775" CREATED="1499696013632" MODIFIED="1499696015745"/>
</node>
<node TEXT="Sean Barakett" FOLDED="true" ID="ID_407951469" CREATED="1499696061522" MODIFIED="1499696094469"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Infrastructure-CICD
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Sean.BARAKETT@bdc.ca" ID="ID_16360796" CREATED="1499696072546" MODIFIED="1499696074508"/>
</node>
<node TEXT="" ID="ID_1675742618" CREATED="1499175517411" MODIFIED="1499175555753" LINK="meetings/Mulesoft%20Week%201%20Schedule.xlsx"/>
</node>
<node TEXT="Mulesoft" FOLDED="true" ID="ID_850458904" CREATED="1499173361633" MODIFIED="1499173364883">
<node TEXT="ryan.paton@mulesoft.com" ID="ID_1525744293" CREATED="1499173383362" MODIFIED="1499173385963"/>
<node TEXT="kevin.smith@mulesoft.com" ID="ID_941977497" CREATED="1499173401106" MODIFIED="1499173403267"/>
<node TEXT="bruno.baloi@mulesoft.com" ID="ID_422061223" CREATED="1499173414402" MODIFIED="1499173416500"/>
<node TEXT="mark.levinson@mulesoft.com" ID="ID_768667748" CREATED="1499180202793" MODIFIED="1499180205082"/>
</node>
</node>
<node TEXT="Project Code" FOLDED="true" POSITION="left" ID="ID_1594397618" CREATED="1499175500059" MODIFIED="1499175504894">
<edge COLOR="#00ffff"/>
<node TEXT="Bus Dev Bank Canada_Implementation_Q_24333" ID="ID_1620370056" CREATED="1499175507059" MODIFIED="1499175508869"/>
</node>
<node TEXT="Links" FOLDED="true" POSITION="left" ID="ID_794442800" CREATED="1499174220346" MODIFIED="1499174223422">
<edge COLOR="#00ff00"/>
<node TEXT="https://scportal.bloomfire.com/login" ID="ID_1882007552" CREATED="1499174224786" MODIFIED="1499174233777">
<icon BUILTIN="closed"/>
</node>
</node>
</node>
</map>
