<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Mulesoft Enterprise Security" FOLDED="false" ID="ID_1471927115" CREATED="1499719541139" MODIFIED="1499719557147" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="3" RULE="ON_BRANCH_CREATION"/>
<node TEXT="features" POSITION="right" ID="ID_1081611305" CREATED="1499719559237" MODIFIED="1499719566342">
<edge COLOR="#ff0000"/>
<node TEXT="Mule Secure Token Service (STS) Oauth 2.0 Provider&#xa;Mule Credentials Vault&#xa;Mule Message Encryption Processor&#xa;Mule Digital Signature Processor&#xa;Mule Filter Processor&#xa;Mule CRC32 Processor" ID="ID_1583898037" CREATED="1499719630342" MODIFIED="1499719632606"/>
</node>
<node TEXT="new features" POSITION="right" ID="ID_1881812195" CREATED="1499719695300" MODIFIED="1499719700445">
<edge COLOR="#0000ff"/>
<node TEXT="Mule Security Manager" ID="ID_547171795" CREATED="1499719702780" MODIFIED="1499719760180" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/configuring-the-spring-security-manager">
<node TEXT="client authentication and authorization on inbound requests as well as credential mapping for outbound calls" ID="ID_1029606560" CREATED="1499719737317" MODIFIED="1499719739389"/>
</node>
<node TEXT="LDAP" ID="ID_649369529" CREATED="1499719763773" MODIFIED="1499720074108" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/setting-up-ldap-provider-for-spring-security">
<node TEXT="and third party Identity Management" ID="ID_1567991344" CREATED="1499719769333" MODIFIED="1499719781734"/>
</node>
<node TEXT="Validation of inbound requests through the SAML 2.0 federated identity standard." ID="ID_1115982480" CREATED="1499719809080" MODIFIED="1499720041458" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/enabling-ws-security"/>
<node TEXT="SFTP transport that enables Mule flows to read and write to remote directories over SSH protocol." ID="ID_244907456" CREATED="1499720094559" MODIFIED="1499720148833"/>
</node>
<node TEXT="Tools" POSITION="right" ID="ID_1092562831" CREATED="1499721161592" MODIFIED="1499721164745">
<edge COLOR="#00ff00"/>
<node TEXT="Mule Secure Token Service (STS) Oauth 2.0 Provider" ID="ID_1205579896" CREATED="1499721174184" MODIFIED="1499721197609">
<node TEXT="" ID="ID_241684724" CREATED="1499721376893" MODIFIED="1499721376893"/>
<node ID="ID_767118899" CREATED="1499721378388" MODIFIED="1499721378388"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <span charset="utf-8" style="color: rgb(58, 59, 60); font-family: OpenSans, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; letter-spacing: -0.14px; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(58, 59, 60)" face="OpenSans, Helvetica, Arial, sans-serif" size="14px">Mule can apply Oauth 2.0 security to a REST Web service provider or consumer. OAuth uses tokens to ensure that a resource owner never has to share credentials, such as a username or password, with a 3rd-party Web service.</font></span>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Apply Oauth 2.0 to my Mule web service provider" ID="ID_441562140" CREATED="1499721505278" MODIFIED="1499721555611" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-secure-token-service"/>
<node TEXT="solutions using Mule Secure Token Service" ID="ID_1974013698" CREATED="1499731944899" MODIFIED="1499731965465">
<node TEXT="Authentication to protect an API from Clients" ID="ID_1349366279" CREATED="1499732040369" MODIFIED="1499732597886"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="rgb(58, 59, 60)" face="OpenSans, Helvetica, Arial, sans-serif" size="14px">The OAuth 2.0 solution is provided by a connector, such as the HTTP or Salesforce connector</font>
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_762535122" CREATED="1499731982607" MODIFIED="1499733034867"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <span charset="utf-8" style="color: rgb(58, 59, 60); font-family: OpenSans, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; letter-spacing: -0.14px; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(58, 59, 60)" face="OpenSans, Helvetica, Arial, sans-serif" size="14px">Client Authentication for a CloudHub API</font></span>
  </body>
</html>

</richcontent>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="rgb(58, 59, 60)" face="OpenSans, Helvetica, Arial, sans-serif" size="14px">The Mule Secure Token Service can provide a solution for APIs and apps hosted by CloudHub.</font>
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Object Store Limitation" ID="ID_1697350755" CREATED="1499733036167" MODIFIED="1499733058854" LINK="https://docs.mulesoft.com/runtime-manager/managing-application-data-with-object-stores#semantics-and-storage-limits"/>
</node>
<node ID="ID_1062921982" CREATED="1499732005074" MODIFIED="1499732291269"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <span charset="utf-8" style="color: rgb(58, 59, 60); font-family: OpenSans, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; letter-spacing: -0.14px; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(58, 59, 60)" face="OpenSans, Helvetica, Arial, sans-serif" size="14px">Client App authentication required by a server</font></span>
  </body>
</html>

</richcontent>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span charset="utf-8" style="color: rgb(58, 59, 60); font-family: OpenSans, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; letter-spacing: -0.14px; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(58, 59, 60)" face="OpenSans, Helvetica, Arial, sans-serif" size="14px">The OAuth 2.0 solution is provided by an API Manager policy</font></span>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="Mule Credentials Vault" ID="ID_1759778634" CREATED="1499721379309" MODIFIED="1499721634313" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-credentials-vault"/>
<node TEXT="Mule Message Encryption Processor" ID="ID_603162538" CREATED="1499721664556" MODIFIED="1499721700424" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-message-encryption-processor"/>
<node TEXT="Mule Digital Signature Processor" ID="ID_171255571" CREATED="1499721810174" MODIFIED="1499721822134"/>
<node TEXT="Mule Filter Processor" ID="ID_933209684" CREATED="1499721832110" MODIFIED="1499721841264"/>
<node TEXT="Mule CRC32 processor" ID="ID_497985887" CREATED="1499721843102" MODIFIED="1499721851247"/>
<node TEXT="examples" ID="ID_1841068810" CREATED="1499724325999" MODIFIED="1499724330344">
<node TEXT="Anypoint Enterprise Security Example application" ID="ID_58745740" CREATED="1499724331575" MODIFIED="1499724373083" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/anypoint-enterprise-security-example-application"/>
<node TEXT="Mule STS OAuth 2.0 example application" ID="ID_1754803471" CREATED="1499724387456" MODIFIED="1499724425977" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-sts-oauth-2.0-example-application"/>
</node>
</node>
</node>
</map>
