<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Cache" FOLDED="false" ID="ID_1427903550" CREATED="1506993321240" MODIFIED="1506993380905" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="6" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Caching Strategy" POSITION="right" ID="ID_1712729203" CREATED="1506994575422" MODIFIED="1506994581907">
<edge COLOR="#00ffff"/>
<node ID="ID_1847365990" CREATED="1506994617677" MODIFIED="1506994649874"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The caching strategy defines the actions a cache scope takes when a message enters its sub flow.
    </p>
    <ul>
      <li>
        If there is no cached response event (a cache &#8220;miss&#8221;), cache scope processes the message.
      </li>
      <li>
        If there is a cached response event (a cache &#8220;hit&#8221;), cache scope offers the cached response event rather than processing the message.
      </li>
    </ul>
    <p>
      You can customize a global caching strategy in Mule for the cache scopes in your application to follow, or you can use Mule&#8217;s default caching strategy.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Default Caching Strategy" ID="ID_1362761694" CREATED="1506994712502" MODIFIED="1506994723972">
<node TEXT="definition" ID="ID_1590801486" CREATED="1506994971418" MODIFIED="1506994975928">
<node TEXT="https://docs.mulesoft.com/mule-user-guide/v/3.8/cache-scope#defaultcachingstrategy" ID="ID_189761118" CREATED="1506994978114" MODIFIED="1506994996326" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/cache-scope#defaultcachingstrategy"/>
<node TEXT="http://www.tutorialsatoz.com/caching-in-mule-esb-cache-scope/" ID="ID_770519002" CREATED="1507066060410" MODIFIED="1507066075615" LINK="http://www.tutorialsatoz.com/caching-in-mule-esb-cache-scope/"/>
<node TEXT="The default caching strategy uses an InMemoryObjectStore and should only be used for testing" ID="ID_241636473" CREATED="1506995064345" MODIFIED="1506995098722">
<icon BUILTIN="idea"/>
<font BOLD="true"/>
</node>
</node>
<node TEXT="in-memory" ID="ID_1389514119" CREATED="1506993388308" MODIFIED="1506994957979">
<node ID="ID_1614360858" CREATED="1506993664357" MODIFIED="1506993755321"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The <b>default caching strategy</b>&#160;used by the cache scope uses an <b>InMemoryObjectStore</b>, <u>and is only suitable for testing</u>. For example, processing messages with large payloads may quickly exhaust memory storage and slow the processing performance of the flow. In such a case, <u>you may wish to create a global caching strategy that stores cached responses in a different type of object store and prevents memory exhaustion.</u>&#160;<b>Apart from Ehcache you can also use Redis Cache for cache scope using a custom java class</b>
    </p>
  </body>
</html>

</richcontent>
<node ID="ID_390657551" CREATED="1507062111805" MODIFIED="1507062143864"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Configure the following settings for an object store that saves cached responses in the system memory:
    </p>
    <ul>
      <li>
        Store name
      </li>
      <li>
        Maximum number of entries (that is, cached responses)
      </li>
      <li>
        The &#8220;life span&#8221; of a cached response within the object store (i.e. time to live)
      </li>
      <li>
        The expiration interval between polls for expired cached responses
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="managed-store" ID="ID_1200147825" CREATED="1506993403998" MODIFIED="1507061734220">
<node TEXT="Starting in Mule 3.5.0, by default new caching strategies use an object store instance that provides a unified cache for all cluster nodes without extra modifications. The exception is the default caching strategy used by the Cache Scope, which uses the InMemoryObjectStore and is intended for testing only" ID="ID_971385612" CREATED="1506995203759" MODIFIED="1506995206964">
<node ID="ID_993291835" CREATED="1507062184466" MODIFIED="1507062211929"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Configure the following settings for an object store that saves cached responses in a place defined by ListableObjectStore:
    </p>
    <ul>
      <li>
        Store name
      </li>
      <li>
        Persistence of cached responses (true/false)
      </li>
      <li>
        Maximum number of entries (i.e. cached responses)
      </li>
      <li>
        The &#8220;life span&#8221; of a cached response within the object store (i.e. time to live)
      </li>
      <li>
        The expiration interval between polls for expired cached responses
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="ehCache" ID="ID_871221518" CREATED="1506993446095" MODIFIED="1506993760739">
<node TEXT="ref" ID="ID_669979032" CREATED="1506993472512" MODIFIED="1506993474486">
<node TEXT="http://www.ricston.com/blog/cache-scope-ehcache/" ID="ID_1494604217" CREATED="1506993476454" MODIFIED="1506993488740" LINK="http://www.ricston.com/blog/cache-scope-ehcache/"/>
<node TEXT="http://mulehacks.blogspot.com/2013/06/enterprise-caching-with-mule-esb.html" ID="ID_964164451" CREATED="1506993499914" MODIFIED="1506993514673" LINK="http://mulehacks.blogspot.com/2013/06/enterprise-caching-with-mule-esb.html"/>
<node TEXT="http://blogs.mulesoft.com/dev/newbie/guest-post-exposing-cxf-webservice-mule-cache/" ID="ID_917359778" CREATED="1506993528253" MODIFIED="1506993539942" LINK="http://blogs.mulesoft.com/dev/newbie/guest-post-exposing-cxf-webservice-mule-cache/"/>
</node>
<node TEXT="about" ID="ID_1740760403" CREATED="1506993564245" MODIFIED="1506993568790">
<node TEXT="Ehcache is one of the better way to get enhanced cached mechanism." ID="ID_576807178" CREATED="1506993571373" MODIFIED="1506993575384"/>
</node>
</node>
<node TEXT="REDIS" ID="ID_1171904112" CREATED="1506993762959" MODIFIED="1506993822999">
<node TEXT="ref" ID="ID_1723594824" CREATED="1506993825327" MODIFIED="1506993827514">
<node TEXT="https://dzone.com/articles/mule-caching-strategy-with-redis-cache" ID="ID_1209529516" CREATED="1506993861341" MODIFIED="1506993872919" LINK="https://dzone.com/articles/mule-caching-strategy-with-redis-cache"/>
</node>
</node>
</node>
<node TEXT="custom-object-store" ID="ID_109575386" CREATED="1507061833804" MODIFIED="1507061875588">
<node TEXT="Create custom class to instruct Mule where and how to store cached responses" ID="ID_1138680745" CREATED="1507061965469" MODIFIED="1507061969313"/>
<node TEXT="ref" ID="ID_1949743649" CREATED="1507065127175" MODIFIED="1507065129832">
<node TEXT="http://www.archive.ricston.com/blog/cache-scope-ehcache/" ID="ID_117210376" CREATED="1507065132223" MODIFIED="1507065216058" LINK="http://www.archive.ricston.com/blog/cache-scope-ehcache/"/>
</node>
</node>
<node TEXT="simple-text-file-store" ID="ID_240598418" CREATED="1507061860922" MODIFIED="1507061867400">
<node ID="ID_1665627901" CREATED="1507062287223" MODIFIED="1507062318710"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Configure the following settings for an object store that saves cached responses in file:
    </p>
    <ul>
      <li>
        Store name
      </li>
      <li>
        Maximum number of entries (i.e. cached responses)
      </li>
      <li>
        The &#8220;life span&#8221; of a cached response within the object store (i.e. time to live)
      </li>
      <li>
        The expiration interval between polls for expired cached responses
      </li>
      <li>
        The name and location of the file in which the object store saves cached responses
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Mule" POSITION="left" ID="ID_1776385522" CREATED="1506994188001" MODIFIED="1506994194195">
<edge COLOR="#ff00ff"/>
<node TEXT="definition" ID="ID_975041388" CREATED="1506994196082" MODIFIED="1506994200439">
<node TEXT="The Cache Scope is a Mule feature for storing and reusing frequently called data. The Cache Scope saves on time and processing load" ID="ID_1118979026" CREATED="1506994202455" MODIFIED="1506994205846">
<node ID="ID_1470651864" CREATED="1506994486070" MODIFIED="1506994514012"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It is particularly effective for:
    </p>
    <ul>
      <li>
        Processing repeated requests for the same information.
      </li>
      <li>
        Processing requests for information that involve large, non-consumable message payloads
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
<node ID="ID_599896646" CREATED="1506994265086" MODIFIED="1506994348955"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      You can put any number of message processors into a cache scope and configure the caching strategy to store the responses (which contain the payload of the response message) produced by the processing that occurs within the scope. <u>Mule&#8217;s default caching strategy defines how data are stored and reused</u>, but if you want to adjust cache behavior, you can customize a global caching strategy in Mule and make it available for use by all cache scopes in your application.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="list of all objectore types" ID="ID_361232155" CREATED="1506995499981" MODIFIED="1506995510072">
<node TEXT="https://www.mulesoft.org/docs/site/3.8.0/apidocs/org/mule/util/store/package-summary.html" ID="ID_1156685540" CREATED="1506995511916" MODIFIED="1506995526219" LINK="https://www.mulesoft.org/docs/site/3.8.0/apidocs/org/mule/util/store/package-summary.html"/>
</node>
<node TEXT="MuleObjectStoreManager" ID="ID_1316260769" CREATED="1506995545933" MODIFIED="1506995556283">
<node TEXT="https://www.mulesoft.org/docs/site/3.8.0/apidocs/org/mule/util/store/MuleObjectStoreManager.html" ID="ID_122522021" CREATED="1506995558767" MODIFIED="1506995568894" LINK="https://www.mulesoft.org/docs/site/3.8.0/apidocs/org/mule/util/store/MuleObjectStoreManager.html"/>
</node>
</node>
<node TEXT="Object Store Module" POSITION="left" ID="ID_1208030540" CREATED="1507062646235" MODIFIED="1507062653636">
<edge COLOR="#7c0000"/>
<node TEXT="ref" ID="ID_1321034746" CREATED="1507062668199" MODIFIED="1507062669918">
<node TEXT="https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-object-stores" ID="ID_782430260" CREATED="1507062878584" MODIFIED="1507062891435" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-object-stores"/>
<node TEXT="https://docs.mulesoft.com/mule-user-guide/v/3.8/object-store-module-reference" ID="ID_1047029681" CREATED="1507062671402" MODIFIED="1507062683810" LINK="https://docs.mulesoft.com/mule-user-guide/v/3.8/object-store-module-reference"/>
<node TEXT="https://dzone.com/articles/object-store-with-mule-esb" ID="ID_115102096" CREATED="1507064700025" MODIFIED="1507064712696" LINK="https://dzone.com/articles/object-store-with-mule-esb"/>
</node>
<node TEXT="To use the object Store module in Anypoint Studio, you must first download the Object Store Connector from the Anypoint Exchange." ID="ID_1568552020" CREATED="1507062712958" MODIFIED="1507062716124">
<node TEXT="Object stores are used to store an object. Mule uses object stores whenever it needs data persistent for later retrieval. Mule uses object stores internally for various filters, routers, and other processors that need to store state between messages. By default, the object store connector not present in Mule palette. So you can install it from Anypoint exchange." ID="ID_1927259470" CREATED="1507064431960" MODIFIED="1507064435736"/>
</node>
<node TEXT="Operations" ID="ID_1555924203" CREATED="1507064514755" MODIFIED="1507064519496">
<node ID="ID_351429904" CREATED="1507064523082" MODIFIED="1507064678147"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The object store connector can perform various operations like Contains, Dual store, Remove, Retrieve, Retrieve all keys, Retrieve and Store and Store.
    </p>
    <ul>
      <li>
        <b>Contains</b>&#160;is used to verify if given key is present in the object store.
      </li>
      <li>
        <b>Dual store</b>&#160;stores a value using a key and also stores a key using a value.
      </li>
      <li>
        <b>Remove</b>&#160;is to remove the respective key from the object store.
      </li>
      <li>
        <b>Retrieve</b>&#160;is used to retrieve an object from the object store and make it available in the specified property scope of a Mule Message.
      </li>
      <li>
        <b>Retrieve all keys</b>&#160;will return a list of all keys present in the object store.
      </li>
      <li>
        <b>Retrieve and Store</b>&#160;is used to retrieve and store an object in the same operation.
      </li>
      <li>
        <b>Store</b>&#160;is used to store the object in an object store.
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Types" ID="ID_1538879714" CREATED="1507064335612" MODIFIED="1507064339503">
<node TEXT="in-memory store" ID="ID_1862391381" CREATED="1507064352778" MODIFIED="1507064358960">
<node TEXT="This allows you to store objects in memory at runtime. Generally, objects are lost when runtime is shutdown." ID="ID_1351785401" CREATED="1507064757688" MODIFIED="1507064760228"/>
</node>
<node TEXT="persistent store" ID="ID_370832956" CREATED="1507064359986" MODIFIED="1507064365772">
<node TEXT="Mule persists data when an object store is explicitly configured to be persistent. Mule creates a default persistent store in the file system." ID="ID_995119947" CREATED="1507064781456" MODIFIED="1507064784375"/>
</node>
</node>
</node>
</node>
</map>
