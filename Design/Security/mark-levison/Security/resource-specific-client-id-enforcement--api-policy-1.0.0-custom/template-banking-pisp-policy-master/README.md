# Resource Specific Client ID Enforcement Policy

This [custom policy](https://docs.mulesoft.com/api-manager/creating-a-policy-walkthrough) enforces use of `client_id` and `client_secret` as Basic authorization for specific resources on the API configurable with regular expression. The request must come from registered client via Anypoint Platform API Manager in Developer portal section ([learn more](https://docs.mulesoft.com/api-manager/browsing-and-accessing-apis))

If the request does not contain valid authentication credentials, the policy rejects the request and **401 Unauthorized** HTTP status code is returned.


##Request requirements:
HTTP Authorization header must have the following form: `Basic QWxhZGRpbjpPcGVuU2VzYW1l`

The header content  starts with 'Basic ' and is folowed by the result of `Base64(client_id + ":" + client_secret)` 


	

## Configuration
The policy configuration contains single mandatory parameter:
	  
+  **resourceRegex** - Regular expression that identifies the resource(s) relative to base URI on the API this policy is applied to (e.g. '/atms/*' - without quotes).


Example values:

 -  `/atms/*` 
 -  `/atms/*|/branches/*`


