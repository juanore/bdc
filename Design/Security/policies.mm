<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Policies" FOLDED="false" ID="ID_1543637143" CREATED="1501267661348" MODIFIED="1501267750949" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7" RULE="ON_BRANCH_CREATION"/>
<node TEXT="apply policies to" POSITION="right" ID="ID_1034942184" CREATED="1501267789130" MODIFIED="1501267796652">
<edge COLOR="#ff0000"/>
<node TEXT="An APIkit project" ID="ID_680583426" CREATED="1501267840010" MODIFIED="1501267911986"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      For example, deploy the APIkit project to Anypoint Platform using API auto-discovery and apply a policy.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="An API running on CloudHub" ID="ID_461649539" CREATED="1501267914581" MODIFIED="1501267956850"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Design an API on Anypoint Platform, configure a proxy for Cloudhub, and apply a policy.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="An API deployed to a private or cloud-based Mule Runtime 3.8.x or API Gateway Runtime 2.x" ID="ID_557912438" CREATED="1501267980414" MODIFIED="1501267983256"/>
</node>
<node TEXT="enhancing security" POSITION="right" ID="ID_1187603490" CREATED="1501268184187" MODIFIED="1501268190620">
<edge COLOR="#00ff00"/>
<node TEXT="In Mule Runtime 3.8.0 and later, you can enhance security through policies by using Gatekeeper. Gatekeeper disables an API until all online policies are applied." ID="ID_739999267" CREATED="1501268191811" MODIFIED="1501268200213"/>
</node>
<node TEXT="Client ID Enforcement" POSITION="right" ID="ID_1684174928" CREATED="1501269218764" MODIFIED="1501269228278">
<edge COLOR="#ff00ff"/>
<node ID="ID_184243949" CREATED="1501269255301" MODIFIED="1501269325755"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>If the credentials are valid, the policy grants access to the deployed API implementation; otherwise, the policy denies access. <u>Credentials are granted per version</u>. If an application obtains credentials to access v1 of the API, the application cannot use the same credentials to access v2 of the API. The application needs to explicitly request access for v2 of the API even through the application credentials are the same as those for v1.</b>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Resource-Level Policy and SLA Tiers" POSITION="right" ID="ID_1349429785" CREATED="1501269838038" MODIFIED="1501269865344" LINK="https://docs.mulesoft.com/api-manager/tutorial-manage-an-api">
<edge COLOR="#00ffff"/>
<node TEXT="A RAML-based API is required for applying a policy to a resource." ID="ID_780517295" CREATED="1501269881551" MODIFIED="1501269900817"/>
<node TEXT="Unsuported Policies" ID="ID_1775773756" CREATED="1501270079831" MODIFIED="1501270133461">
<node ID="ID_1094234348" CREATED="1501270150053" MODIFIED="1501270184823"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      You cannot apply the following policies to resources:
    </p>
    <ul>
      <li>
        Cross-Origin Resource Sharing
      </li>
      <li>
        LDAP Security Manager
      </li>
      <li>
        Simple Security Manager
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Usage Scenarios" ID="ID_733578855" CREATED="1501270265768" MODIFIED="1501272192777" LINK="https://docs.mulesoft.com/api-manager/resource-level-policies-about#usage-scenarios">
<node ID="ID_1031546418" CREATED="1501270293953" MODIFIED="1501270293953"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <div charset="utf-8" class="paragraph" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(51, 51, 51); font-family: OpenSans, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
        The uses for resource level policies are limited mainly by your imagination. Here are a few possibilities:
      </p>
    </div>
  </body>
</html>
</richcontent>
<node ID="ID_1929677910" CREATED="1501270293956" MODIFIED="1501270293956"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 1em; line-height: 1.6; list-style-position: outside; font-family: inherit">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          Applying policies to specific resources
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_205521611" CREATED="1501270293958" MODIFIED="1501270293958"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 1em; line-height: 1.6; list-style-position: outside; font-family: inherit">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          Securing a subset of an API
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1552965574" CREATED="1501270293960" MODIFIED="1501270293960"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 1em; line-height: 1.6; list-style-position: outside; font-family: inherit">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          Setting different limits on resources
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="what is a policy" FOLDED="true" POSITION="left" ID="ID_807426682" CREATED="1501267798978" MODIFIED="1501267807662">
<edge COLOR="#0000ff"/>
<node TEXT="A policy is a mechanism for enforcing filters on traffic. These filters generally control authentication, access, allotted consumption, and service level access (SLA)" ID="ID_443071321" CREATED="1501267809005" MODIFIED="1501267817031"/>
<node TEXT="By default, a policy applies to the entire API, filtering traffic requests to every resource and method." ID="ID_81956980" CREATED="1501268108688" MODIFIED="1501268114882"/>
</node>
<node TEXT="About OAuth 2.0" POSITION="left" ID="ID_1263714566" CREATED="1501428836404" MODIFIED="1501428914008" LINK="https://docs.mulesoft.com/api-manager/aes-oauth-faq">
<edge COLOR="#00007c"/>
<node TEXT="OAuth providers and policy pairs" ID="ID_1219399989" CREATED="1501428989606" MODIFIED="1501429003473">
<node TEXT="" ID="ID_1554698568" CREATED="1501429031246" MODIFIED="1501429031246">
<node ID="ID_1594829831" CREATED="1501429035116" MODIFIED="1501429035116"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul charset="utf-8" style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 14px; line-height: 1.6; list-style-position: outside; font-family: OpenSans, Helvetica, Arial, sans-serif; color: rgb(51, 51, 51); font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          PingFederate provider and PingFederate OAuth Token Enforcement policy
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_805781115" CREATED="1501429035119" MODIFIED="1501429035119"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul charset="utf-8" style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 14px; line-height: 1.6; list-style-position: outside; font-family: OpenSans, Helvetica, Arial, sans-serif; color: rgb(51, 51, 51); font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          OpenAM provider and OpenAM OAuthToken Enforcement policy
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1643634292" CREATED="1501429035126" MODIFIED="1501429035126"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul charset="utf-8" style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 14px; line-height: 1.6; list-style-position: outside; font-family: OpenSans, Helvetica, Arial, sans-serif; color: rgb(51, 51, 51); font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          Mule OAuth 2.0 provider and OAuth 2.0 Access Token Enforcement Using External Provider policy
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="exchange" FOLDED="true" ID="ID_1616548106" CREATED="1501429196041" MODIFIED="1501429199523">
<node TEXT="OAuth 2.0 JWE access token enforcement | API policy" ID="ID_636599456" CREATED="1501429206361" MODIFIED="1501429439630" LINK="https://anypoint.mulesoft.com/exchange/68ef9520-24e9-4cf2-b2f5-620025690913/oauth-20-jwe-access-token-enforcement--api-policy/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      custom
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="two factor authentication | API policy" ID="ID_482405696" CREATED="1501432195565" MODIFIED="1501432229840" LINK="https://anypoint.mulesoft.com/exchange/68ef9520-24e9-4cf2-b2f5-620025690913/two-factor-auth-policy/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      custom
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Resource specific Client ID enforcement | API policy" ID="ID_1019665464" CREATED="1501432254190" MODIFIED="1501432289278" LINK="https://anypoint.mulesoft.com/exchange/68ef9520-24e9-4cf2-b2f5-620025690913/resource-specific-client-id-enforcement--api-policy/"/>
<node TEXT="External OAuth 2.0 server for Anypoint Platform" ID="ID_802657165" CREATED="1501429286491" MODIFIED="1501429457772" LINK="https://anypoint.mulesoft.com/exchange/org.mule.templates/api-gateway-external-oauth2-provider/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      template
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="External OAuth 2.0 server for Anypoint Platform using LDAP" ID="ID_52331890" CREATED="1501429341053" MODIFIED="1501429465013" LINK="https://anypoint.mulesoft.com/exchange/org.mule.templates/api-gateway-external-oauth2-provider/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      template
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="OAuth 2.0 security scheme" ID="ID_1387425844" CREATED="1501429487187" MODIFIED="1501429607601" LINK="https://anypoint.mulesoft.com/exchange/68ef9520-24e9-4cf2-b2f5-620025690913/training-oauth20-security-scheme/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      training
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="OAuth 2.0 Authorization Code using the HTP connector" ID="ID_1239355106" CREATED="1501429543604" MODIFIED="1501429701392" LINK="https://anypoint.mulesoft.com/exchange/org.mule.examples/oauth2-authorization-code-using-the-HTTP-connector/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      template
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Authorization Server | implementation template" ID="ID_112888993" CREATED="1501429630224" MODIFIED="1501429714599" LINK="https://anypoint.mulesoft.com/exchange/org.mule.templates/template-banking-authorization-server/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      template
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
