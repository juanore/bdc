<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Access Management" FOLDED="false" ID="ID_80664366" CREATED="1502285474093" MODIFIED="1502285496483" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="SSO Prerequisites" POSITION="right" ID="ID_536892249" CREATED="1502286895550" MODIFIED="1502286905675">
<edge COLOR="#00ff00"/>
<node ID="ID_833014145" CREATED="1502286951611" MODIFIED="1502287208235"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Observe the following prerequisites:
    </p>
    <p>
      
    </p>
    <ul>
      <li>
        <b>Configure your IdP to send the your user name and email in your assertion.</b>
      </li>
      <li>
        <b>Configure Anypoint Platform to map the user name and email to the expected attribute name. Otherwise, the login fails with a 403 unauthorized error message.</b>
      </li>
      <li>
        The identity provider (IdP) issues the SAML assertion as an XML file. Ensure that this assertion is signed, but unencrypted, for verification of its integrity because the assertion itself is POSTed through HTTPS.
      </li>
      <li>
        Prepare users for the following changes:

        <ul>
          <li>
            User accounts that you create before configuring your federated organization remain. However, your users can login only through the Anypoint Platform Sign In page, not the IdP redirected custom login page.
          </li>
          <li>
            With external identity enabled, the invite button is disabled and no new non-federated users can be added. Existing non-federated users can continue to work normally, with some exceptions:
          </li>
          <li>
            If a user session times out, the user is redirected to the federated identity login page instead of the generic one. Links and bookmarks that identify the organization redirect the user to the federated login page, which fails for non-federated users.
          </li>
          <li>
            If you configure an IdP to handle user information assertion, users need to log into Anypoint Platform at the following location:<br/><b>https://anypoint.mulesoft.com/accounts/login/{yourorgDomain}</b>
          </li>
        </ul>
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="SSO Limitations" POSITION="right" ID="ID_731591328" CREATED="1502286906363" MODIFIED="1502286913347">
<edge COLOR="#ff00ff"/>
<node ID="ID_606015982" CREATED="1502287294187" MODIFIED="1502287354007"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Federated users cannot use platform APIs. </b>
      </li>
      <li>
        <b>OpenID Connect does not support single logout. </b>
      </li>
      <li>
        <b>OpenID Connect does not support role mapping.</b>
      </li>
      <li>
        If a SAML user belongs to certain groups, Anypoint Platform does not automatically grant equivalent roles in the organization.
      </li>
      <li>
        Anypoint Platform does not generate the SAML assertion for the single sign on. Your IdP generates the Sign On URL that you configure.
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="setup" POSITION="right" ID="ID_1525932124" CREATED="1502285514512" MODIFIED="1502285517653">
<edge COLOR="#ff0000"/>
<node TEXT="Required URL" ID="ID_1118819921" CREATED="1502290043607" MODIFIED="1502290049013">
<node ID="ID_1760992629" CREATED="1502290051013" MODIFIED="1502290244617"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table class="tableblock frame-all grid-all spread" style="border-spacing: 0px; background-color: rgb(255,; background-position: 255, 255); background-image: null; background-repeat: repeat; background-attachment: scroll; margin-top: 30px; margin-bottom: 30px; margin-right: 0px; margin-left: 0px; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; width: 653px; color: rgb(51, 51, 51); font-family: OpenSans, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px">
      <tr>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <font color="#000000"><b>URL Name </b></font>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <font color="#000000"><b>Okta Example URL </b></font>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <font color="#000000"><b>OpenAM Example URL </b></font>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <font color="#000000"><b>PingFederate Example URL </b></font>
          </p>
        </td>
      </tr>
      <tr style="background-color: white; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null">
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">Base </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <code style="font-family: Menlo, Monaco, Consolas, Courier New, monospace; font-size: 0.9375em; font-weight: 400; color: rgb(58, 59, 60); padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; background-position: 0px center; background-image: null; background-repeat: repeat; background-attachment: scroll; letter-spacing: 0px; word-spacing: -0.15em; line-height: 1.45; font-style: normal !important"><font face="Menlo, Monaco, Consolas, Courier New, monospace" size="0.9375em" color="#000000"><b>https://example.okta.com/oauth2/v1</b></font></code>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <code style="font-family: Menlo, Monaco, Consolas, Courier New, monospace; font-size: 0.9375em; font-weight: 400; color: rgb(58, 59, 60); padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; background-position: 0px center; background-image: null; background-repeat: repeat; background-attachment: scroll; letter-spacing: 0px; word-spacing: -0.15em; line-height: 1.45; font-style: normal !important"><font face="Menlo, Monaco, Consolas, Courier New, monospace" size="0.9375em" color="#000000"><b>https://example.com/openam/oauth2</b></font></code>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <code style="font-family: Menlo, Monaco, Consolas, Courier New, monospace; font-size: 0.9375em; font-weight: 400; color: rgb(58, 59, 60); padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; background-position: 0px center; background-image: null; background-repeat: repeat; background-attachment: scroll; letter-spacing: 0px; word-spacing: -0.15em; line-height: 1.45; font-style: normal !important"><font face="Menlo, Monaco, Consolas, Courier New, monospace" size="0.9375em" color="#000000"><b>https://example.com:9031</b></font></code>
          </p>
        </td>
      </tr>
      <tr>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">Client Registration </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/clients </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/connect/register </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">N/A </font></b>
          </p>
        </td>
      </tr>
      <tr style="background-color: white; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null">
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">Authorize </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/authorize </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/authorize </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/as/authorization.oauth2 </font></b>
          </p>
        </td>
      </tr>
      <tr>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">Token </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/token </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/access_token </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/as/token.oauth2 </font></b>
          </p>
        </td>
      </tr>
      <tr style="background-color: white; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null">
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">User Info </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/userinfo </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/userinfo </font></b>
          </p>
        </td>
        <td class="tableblock halign-left valign-top" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; font-size: 14px; color: black; display: table-cell; line-height: 1.6; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: solid; border-left-style: none; border-bottom-color: rgb(233, 233, 233); text-align: left; vertical-align: top">
          <p class="tableblock" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 5px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
            <b><font color="#000000">{BASE URL}/idp/userinfo.openid</font></b>
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Client Registration in OpenID Connect" ID="ID_790838502" CREATED="1502289082614" MODIFIED="1502289133943">
<node TEXT="Dynamic Registration" ID="ID_1040966280" CREATED="1502289146384" MODIFIED="1502289765672">
<icon BUILTIN="full-1"/>
<node TEXT="https://docs.mulesoft.com/access-management/conf-openid-connect-task#to-configure-openid-connect-dynamically" ID="ID_1302973977" CREATED="1502290357784" MODIFIED="1502290370852" LINK="https://docs.mulesoft.com/access-management/conf-openid-connect-task#to-configure-openid-connect-dynamically"/>
</node>
<node TEXT="Manual Registration" ID="ID_1853144904" CREATED="1502289154181" MODIFIED="1502289769282">
<icon BUILTIN="full-2"/>
<node TEXT="https://docs.mulesoft.com/access-management/conf-openid-connect-task#to-configure-openid-connect-manually" ID="ID_914857480" CREATED="1502290396670" MODIFIED="1502290411371" LINK="https://docs.mulesoft.com/access-management/conf-openid-connect-task#to-configure-openid-connect-manually"/>
</node>
<node ID="ID_397006691" CREATED="1502289283081" MODIFIED="1502289882009">
<icon BUILTIN="bell"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      MuleSoft verifies support in Anypoint Platform for the following registrations:
    </p>
    <ul>
      <li>
        Clients created dynamically in Okta and OpenAM
      </li>
      <li>
        Clients created manually in Okta, OpenAM, and PingFederate
      </li>
    </ul>
    <p>
      <font color="#0000ff"><b>If you already configured Anypoint Platform as a client application in your identity provider, perform manual registration. Otherwise, if your identity provider supports dynamic client registration, perform dynamic registration</b></font>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="SSO URL" ID="ID_90099252" CREATED="1502290480640" MODIFIED="1502290484530">
<node TEXT="https://anypoint.mulesoft.com/accounts/login/{yourOrgDomain}" ID="ID_925563423" CREATED="1502290486109" MODIFIED="1502290503604">
<icon BUILTIN="password"/>
</node>
</node>
</node>
<node TEXT="reference" POSITION="left" ID="ID_1099097615" CREATED="1502285520762" MODIFIED="1502285525247">
<edge COLOR="#0000ff"/>
<node TEXT="aceess management API" ID="ID_1969119918" CREATED="1502285527841" MODIFIED="1502285559208" LINK="https://anypoint.mulesoft.com/apiplatform/anypoint-platform/#/portals/organizations/68ef9520-24e9-4cf2-b2f5-620025690913/apis/11270/versions/11646/pages/11244"/>
<node TEXT="OpenID tasks" ID="ID_1473363380" CREATED="1502290284659" MODIFIED="1502290291753">
<node TEXT="https://docs.mulesoft.com/access-management/conf-openid-connect-task" ID="ID_181404188" CREATED="1502290293863" MODIFIED="1502290307350" LINK="https://docs.mulesoft.com/access-management/conf-openid-connect-task"/>
</node>
</node>
</node>
</map>
