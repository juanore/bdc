<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="OAuth 2.0" FOLDED="false" ID="ID_943114612" CREATED="1501273129943" MODIFIED="1501273146626" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="6" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Anypoint Platform works with OAUTH solutions" POSITION="right" ID="ID_1548480039" CREATED="1501273242445" MODIFIED="1502417815764">
<edge COLOR="#ff0000"/>
<node TEXT="Accessing an OAuth-protected server from a client app" ID="ID_967873346" CREATED="1501273327935" MODIFIED="1501273360980"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Example: Configure an HTTP or Salesforce connector in the app to access OAuth-protected servers.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Protecting an API from unauthorized calls from client applications" ID="ID_107638902" CREATED="1501273373352" MODIFIED="1501273397418"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Example: Apply an OAuth 2.0 policy and your configured OAuth provider denies access to unauthenticated client apps.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="OAuth provider and policy pairs" POSITION="right" ID="ID_1762811309" CREATED="1501273416113" MODIFIED="1502417835433">
<edge COLOR="#0000ff"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="" ID="ID_865863171" CREATED="1502418089178" MODIFIED="1502418089179">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="PingFederate provider" ID="ID_1131882290" CREATED="1501273445665" MODIFIED="1501273460939">
<node TEXT="PingFederate OAuth Token Enforcement policy" ID="ID_1629379343" CREATED="1501273462354" MODIFIED="1501273488654"/>
</node>
<node TEXT="OpenAM provider" ID="ID_1378361989" CREATED="1501273492795" MODIFIED="1501273500356">
<node TEXT="OpenAM OAuthToken Enforcement policy" ID="ID_168961841" CREATED="1501273502251" MODIFIED="1501273527845"/>
</node>
<node TEXT="" ID="ID_1118590975" CREATED="1502418089176" MODIFIED="1502418089178">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="If you don&#x2019;t want to contract with PingFederate or OpenAM, you need to build a Mule OAuth provider." ID="ID_1953600719" CREATED="1502418089179" MODIFIED="1502418101285"/>
</node>
<node TEXT="Mule OAuth 2.0 provider" ID="ID_431567395" CREATED="1501273535030" MODIFIED="1501273548280">
<node TEXT="OAuth 2.0 Access Token Enforcement using External Provider policy" ID="ID_1451276747" CREATED="1501273549343" MODIFIED="1501273590718"/>
</node>
</node>
<node TEXT="Mule OAuth Dance" POSITION="right" ID="ID_1842866738" CREATED="1501431826677" MODIFIED="1501431835790">
<edge COLOR="#00ffff"/>
<node ID="ID_92221712" CREATED="1501431837133" MODIFIED="1502417939074"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol charset="utf-8" class="arabic" style="margin-top: 14px; margin-right: 0px; margin-bottom: 20px; margin-left: 25px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 14px; line-height: 1.6; list-style-position: outside; font-family: OpenSans, Helvetica, Arial, sans-serif; list-style-type: decimal; color: rgb(51, 51, 51); font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          The client application requests a token from the provider.
        </p>
      </li>
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          The provider returns a token.
        </p>
      </li>
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          The client application includes the token either as an authentication header or a query parameter in a request to the API.
        </p>
      </li>
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          <b>The OAuth 2.0 Access Token Enforcement Using External Provider Policy intercepts this request and communicates with the provider to validate the token. </b>
        </p>
      </li>
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          <b>The validated token is whitelisted and kept on record until expiration. Any further requests that contain this token are not validated against the OAuth provider.</b>
        </p>
      </li>
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          If the token is valid, the request is forwarded to the API.
        </p>
      </li>
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 10px; color: black">
        <p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 3px; padding-left: 0px; font-family: OpenSans, Helvetica, Arial, sans-serif; font-weight: 400; font-size: 14px; line-height: 21px; letter-spacing: -0.01em; color: rgb(58, 59, 60)">
          The API responds to the client application.
        </p>
      </li>
    </ol>
  </body>
</html>

</richcontent>
<node TEXT="" ID="ID_132999681" CREATED="1501431854765" MODIFIED="1501431900706">
<hook URI="images/Mule%20OAuth%20Dance.png" SIZE="0.60913706" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Mule OAuth provider" FOLDED="true" POSITION="left" ID="ID_433128955" CREATED="1501273667092" MODIFIED="1501273679366">
<edge COLOR="#ff00ff"/>
<node TEXT="prerequisites" ID="ID_20210400" CREATED="1501285391796" MODIFIED="1501285396102">
<node TEXT="Mule 3.8.0 runtime or later&#xa;&#xa;API Gateway runtime 2.0 or earlier" ID="ID_918630545" CREATED="1501285397412" MODIFIED="1501285442246"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      one of the above
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="The Mule OAuth provider can run on any application server that is in the same organization as your OAuth-protected API. For example, you can run the Mule provider on premises using a Tomcat server or in the cloud using CloudHub." ID="ID_1880317245" CREATED="1501285511555" MODIFIED="1501285514412"/>
</node>
<node TEXT="reference" ID="ID_298251040" CREATED="1501430341807" MODIFIED="1501430387798" LINK="https://docs.mulesoft.com/api-manager/building-an-external-oauth-2.0-provider-application">
<node TEXT="http://www.bubblecode.net/en/2016/01/22/understanding-oauth2/" ID="ID_439024772" CREATED="1501549881421" MODIFIED="1501549881421" LINK="http://www.bubblecode.net/en/2016/01/22/understanding-oauth2/"/>
<node TEXT="https://tools.ietf.org/html/rfc6749" ID="ID_1801297465" CREATED="1501550251205" MODIFIED="1501550270795" LINK="https://tools.ietf.org/html/rfc6749"/>
<node TEXT="oauth.net" ID="ID_600755833" CREATED="1502326930747" MODIFIED="1502326956894" LINK="https://oauth.net/getting-started/"/>
</node>
<node TEXT="download" ID="ID_273932319" CREATED="1501430456749" MODIFIED="1501430469101" LINK="https://docs.mulesoft.com/api-manager/_attachments/OAuthProviderStudioArchive.zip"/>
</node>
<node TEXT="Basic Knowledge" POSITION="left" ID="ID_1736407022" CREATED="1501549355722" MODIFIED="1501549363335">
<edge COLOR="#7c0000"/>
<node TEXT="Roles" ID="ID_1023914823" CREATED="1501549368019" MODIFIED="1501549371830">
<node TEXT="Client" FOLDED="true" ID="ID_365558924" CREATED="1501549396363" MODIFIED="1502327363004"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      third-party application
    </p>
  </body>
</html>

</richcontent>
<node TEXT="application requesting access to a Resource Server" ID="ID_1710749312" CREATED="1501549467731" MODIFIED="1502327447676">
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
</node>
</node>
<node TEXT="Resource Server" FOLDED="true" ID="ID_1177104645" CREATED="1501549389179" MODIFIED="1502327389183"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the API
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Server hosting protected data.&#xa;Is the API server used to access the user&apos;s information.&#xa;The resource server is the OAuth 2.0 term for your API server. The resource server handles authenticated requests after the application has obtained an access token." ID="ID_294632970" CREATED="1501549446431" MODIFIED="1502390108822">
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
</node>
<node TEXT="verifying access tokens" ID="ID_764411697" CREATED="1502390305944" MODIFIED="1502390427397" LINK="#ID_1964744016"/>
</node>
<node TEXT="Authorization Server" FOLDED="true" ID="ID_1720408816" CREATED="1501549404459" MODIFIED="1501549413172">
<node TEXT="server issuing access token to the client.&#xa;This token will be used for the client to request the resource server.&#xa;The resource server can be the same as the authorization server." ID="ID_328027857" CREATED="1501549525363" MODIFIED="1502327472537">
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
<node TEXT="" ID="ID_762095462" CREATED="1502385979809" MODIFIED="1502386058864">
<hook URI="../Projects/bdc/Design/Security/images/okta_oauth-diagrams_authz-req.png" SIZE="0.6315789" NAME="ExternalObject"/>
</node>
<node TEXT="" ID="ID_1935092635" CREATED="1502386071124" MODIFIED="1502386119933">
<hook URI="../Projects/bdc/Design/Security/images/okta_oauth-diagrams_authn-authz.png" SIZE="0.6968641" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Resource Owner" FOLDED="true" ID="ID_105324657" CREATED="1501549372947" MODIFIED="1502327404855"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The User
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Generally yourself" ID="ID_1644991988" CREATED="1501549424316" MODIFIED="1502327492122">
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
</node>
</node>
</node>
<node TEXT="Register as a Client" FOLDED="true" ID="ID_184456323" CREATED="1501551153361" MODIFIED="1501551161674">
<node TEXT="Since you want to retrieve data from a resource server using OAuth2, you have to register as a client of the authorization server." ID="ID_1592354693" CREATED="1501551314142" MODIFIED="1502327816294">
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
</node>
<node TEXT="parameters" FOLDED="true" ID="ID_243077709" CREATED="1501551339215" MODIFIED="1502327816296">
<cloud COLOR="#f0f0f0" SHAPE="RECT"/>
<node TEXT="Client Registration" ID="ID_381391218" CREATED="1501551345799" MODIFIED="1501551352233">
<node TEXT="Application Name: the application name&#xa;Redirect URLs: URLs of the client for receiving authorization code and access token&#xa;Grant Type(s): authorization types that will be used by the client&#xa;Javascript Origin (optional): the hostname that will be allowed to request the resource server via XMLHttpRequest" ID="ID_673847235" CREATED="1501551385700" MODIFIED="1501551389599"/>
</node>
<node TEXT="Authorization Server Response" FOLDED="true" ID="ID_279943538" CREATED="1501551360964" MODIFIED="1501551371173">
<node TEXT="Client Id: unique random string&#xa;Client Secret: secret key that must be kept confidential" ID="ID_1466293281" CREATED="1501551410557" MODIFIED="1501551414127">
<node TEXT="Client ID&#xa;&#xa;The client_id is a public identifier for apps. Even though it&#x2019;s public, it&#x2019;s best that it isn&#x2019;t guessable by third parties, so many implementations use something like a 32-character hex string. It must also be unique across all clients that the authorization server handles. If the client ID is guessable, it makes it slightly easier to craft phishing attacks against arbitrary applications." ID="ID_870094125" CREATED="1502384177854" MODIFIED="1502384183575"/>
<node TEXT="Client Secret&#xa;&#xa;The client_secret is a secret known only to the application and the authorization server. It must be sufficiently random to not be guessable, which means you should avoid using common UUID libraries which often take into account the timestamp or MAC address of the server generating it. A great way to generate a secure secret is to use a cryptographically-secure library to generate a 256-bit value and converting it to a hexadecimal representation." ID="ID_581850779" CREATED="1502384236168" MODIFIED="1502384241041"/>
</node>
<node TEXT="Storing and Displaying the Client ID and Secret&#xa;&#xa;For each registered application, you&#x2019;ll need to store the public client_id and the private client_secret. Because these are essentially equivalent to a username and password, you should not store the secret in plain text, instead only store an encrypted or hashed version, to help reduce the likelihood of the secret leaking." ID="ID_940524921" CREATED="1502384306704" MODIFIED="1502384312081"/>
<node TEXT="Deleting Applications and Revoking Secrets" ID="ID_943207873" CREATED="1502384398766" MODIFIED="1502384415712"/>
</node>
</node>
</node>
<node TEXT="Authorization Grant Types" ID="ID_688841460" CREATED="1501551450472" MODIFIED="1501551461057">
<node TEXT="Authorization Code Grant" FOLDED="true" ID="ID_290615469" CREATED="1501551479392" MODIFIED="1502329609817">
<icon BUILTIN="bookmark"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="when to use it" FOLDED="true" ID="ID_38526093" CREATED="1501552498067" MODIFIED="1501552503717">
<node ID="ID_506365463" CREATED="1501552505412" MODIFIED="1502382770971"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It should be used as soon as the client is a web server. It allows you to obtain a long-lived access token since it can be renewed with a refresh token (if the authorization server enables it).
    </p>
    <p>
      For apps running on a <font color="#0000ff">web server</font>, <font color="#0000ff">browser-based (single-page apps)</font>&#160;and <font color="#0000ff">mobile apps </font>
    </p>
    <p>
      <b>Using Authorization Code replaces the option of using Implicit grant with browser-based and mobile apps. However, some services will not accept the authorization code exchange without the client secret, so native apps might need to use an alternate method for those services.</b>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="example" FOLDED="true" ID="ID_830639765" CREATED="1501552524189" MODIFIED="1501552526646">
<node TEXT="Resource Owner: you&#xa;Resource Server: a Google server&#xa;Client: any website&#xa;Authorization Server: a Google server" ID="ID_740444299" CREATED="1501552547629" MODIFIED="1501552552719"/>
</node>
<node TEXT="format" ID="ID_1537229206" CREATED="1502378709428" MODIFIED="1502378712865">
<node TEXT="authorization URL" ID="ID_580763259" CREATED="1502378719945" MODIFIED="1502379595210">
<icon BUILTIN="full-1"/>
<node TEXT="https://authorization-server.com/oauth/authorize&#xa;?client_id=a17c21ed&#xa;&amp;response_type=code&#xa;&amp;state=5ca75bd30&#xa;&amp;redirect_uri=https%3A%2F%2Foauth2client.com%2Fauth" FOLDED="true" ID="ID_1681000103" CREATED="1502378770554" MODIFIED="1502379369926"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span charset="utf-8" style="color: rgb(51, 51, 51); font-family: droid-serif, serif; font-size: 20px; font-style: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(51, 51, 51)" face="droid-serif, serif" size="20px">Combine all of these query string parameters into the login URL, and direct the user&#8217;s browser there. Typically apps will put these parameters into a login button, or will send this URL as an HTTP redirect from the app&#8217;s own login URL.</font></span>
    </p>
  </body>
</html>

</richcontent>
<node ID="ID_87742936" CREATED="1502378937120" MODIFIED="1502379063520"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client_id</b>
    </p>
    <p>
      The client_id is the identifier for your app. You will have received a client_id when first registering your app with the service.
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1954175709" CREATED="1502379089128" MODIFIED="1502379220605"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>response_type</b>
    </p>
    <p>
      response_type is set to code indicating that you want an authorization code as the response.
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_864529082" CREATED="1502379122225" MODIFIED="1502379239242"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>redirect_uri (optional)</b>
    </p>
    <p>
      The redirect_uri may be optional depending on the API, but is highly recommended. This is the URL to which you want the user to be redirected after the authorization is complete. This must match the redirect URL that you have previously registered with the service.
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1836324391" CREATED="1502379143906" MODIFIED="1502379258967"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>scope (optional)</b>
    </p>
    <p>
      Include one or more scope values (space-separated) to request additional levels of access. The values will depend on the particular service.
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1039594376" CREATED="1502379193752" MODIFIED="1502379274599"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>state (recommended)</b>
    </p>
    <p>
      The state parameter serves two functions. When the user is redirected back to your app, whatever value you include as the state will also be included in the redirect. This gives your app a chance to persist data between the user being directed to the authorization server and back again, such as using the state parameter as a session key. This may be used to indicate what action in the app to perform after authorization is complete, for example, indicating which of your app&#8217;s pages to redirect to after authorization. This also serves as a CSRF protection mechanism. When the user is redirected back to your app, double check that the state value matches what you set it to originally. This will ensure an attacker can&#8217;t intercept the authorization flow.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="https://authorization-server.com/auth?response_type=code&#xa;&amp;client_id=29352735982374239857&#xa;&amp;redirect_uri=https://example-app.com/callback&#xa;&amp;scope=create+delete&#xa;&amp;state=xcoivjuywkdkhvusuye3kch" ID="ID_1073518337" CREATED="1502385143895" MODIFIED="1502385152241"/>
</node>
<node TEXT="redirect with authorization code" ID="ID_505986536" CREATED="1502379496532" MODIFIED="1502379598614">
<icon BUILTIN="full-2"/>
<node TEXT="https://example-app.com/cb?code=Yzk5ZDczMzRlNDEwY" ID="ID_1592667853" CREATED="1502379512796" MODIFIED="1502379517485"/>
</node>
<node TEXT="exchange auth code for access token" ID="ID_973373456" CREATED="1502379557157" MODIFIED="1502379601724">
<icon BUILTIN="full-3"/>
<node TEXT="POST /oauth/token HTTP/1.1&#xa;Host: authorization-server.com&#xa; &#xa;code=Yzk5ZDczMzRlNDEwY&#xa;&amp;grant_type=code&#xa;&amp;redirect_uri=https://example-app.com/cb&#xa;&amp;client_id=mRkZGFjM&#xa;&amp;client_secret=ZGVmMjMz" ID="ID_38932152" CREATED="1502379576029" MODIFIED="1502379580535"/>
</node>
<node TEXT="response" ID="ID_341443714" CREATED="1502379643712" MODIFIED="1502379658903">
<icon BUILTIN="full-4"/>
<node TEXT="{&#xa;  &quot;access_token&quot;: &quot;AYjcyMzY3ZDhiNmJkNTY&quot;,&#xa;  &quot;refresh_token&quot;: &quot;RjY2NjM5NzA2OWJjuE7c&quot;,&#xa;  &quot;token_type&quot;: &quot;bearer&quot;,&#xa;  &quot;expires&quot;: 3600&#xa;}" ID="ID_979786291" CREATED="1502379648160" MODIFIED="1502379652985">
<node TEXT="access_token (required) The access token string as issued by the authorization server." ID="ID_1585068057" CREATED="1502388533926" MODIFIED="1502388540783"/>
<node ID="ID_1360228893" CREATED="1502388632041" MODIFIED="1502388665511"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      refresh_token (optional) If the access token will expire, then it is useful to return a refresh token which applications can use to obtain another access token. <b><font color="#0000ff">However, tokens issued with the implicit grant cannot be issued a refresh token.</font></b>
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="token_type (required) The type of token this is, typically just the string &#x201c;bearer&#x201d;" ID="ID_1017466297" CREATED="1502388567703" MODIFIED="1502388573392"/>
<node TEXT="expires_in (recommended) If the access token expires, the server should reply with the duration of time the access token is granted for." ID="ID_46436591" CREATED="1502388590863" MODIFIED="1502388599569"/>
<node TEXT="scope (optional) If the scope the user granted is identical to the scope the app requested, this parameter is optional. If the granted scope is different from the requested scope, such as if the user modified the scope, then this parameter is required." ID="ID_1416210511" CREATED="1502388735236" MODIFIED="1502388741213"/>
</node>
</node>
</node>
<node TEXT="scenario" FOLDED="true" ID="ID_1369654147" CREATED="1501552586862" MODIFIED="1501552590192">
<node ID="ID_1198325552" CREATED="1501552609159" MODIFIED="1501552629569"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        A website wants to obtain information about your Google profile.
      </li>
      <li>
        You are redirected by the client (the website) to the authorization server (Google).
      </li>
      <li>
        If you authorize access, the authorization server sends an authorization code to the client (the website) in the callback response.
      </li>
      <li>
        Then, this code is exchanged against an access token between the client and the authorization server.
      </li>
      <li>
        The website is now able to use this access token to query the resource server (Google again) and retrieve your profile data.
      </li>
    </ol>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="pros/cons" FOLDED="true" ID="ID_303430657" CREATED="1501552653264" MODIFIED="1501552659025">
<node TEXT="This is the ideal scenario and the safer one because the access token is not passed on the client side (web browser in our example)." ID="ID_1672932334" CREATED="1501552661048" MODIFIED="1501552670312">
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="sequence diagram" FOLDED="true" ID="ID_1350561243" CREATED="1501552677176" MODIFIED="1501552682410">
<node TEXT="" ID="ID_1059536960" CREATED="1501552719121" MODIFIED="1501552770146">
<hook URI="images/auth_code_flow.png" SIZE="0.9661836" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="vulnerability" FOLDED="true" ID="ID_1398108308" CREATED="1501554556185" MODIFIED="1501554646673">
<icon BUILTIN="stop-sign"/>
<node TEXT="There is a vulnerability in this flow that allows an attacker to steal a user&#x2019;s account under certain conditions." ID="ID_1028528815" CREATED="1501554565185" MODIFIED="1501554570619"/>
<node TEXT="workaround" ID="ID_1149003554" CREATED="1501554584673" MODIFIED="1501554626920">
<icon BUILTIN="idea"/>
<node TEXT="There is a way to prevent this by adding a &#x201c;state&#x201d; parameter. The latter is only recommended and not required in the specifications. If the client sends this parameter when requesting an authorization code, it will be returned unchanged by the authorization server in the response and will be compared by the client before the exchange of the authorization code against the access token." ID="ID_1077219602" CREATED="1501554615374" MODIFIED="1501554619720"/>
</node>
</node>
</node>
<node TEXT="Implicit Grant" FOLDED="true" ID="ID_338726882" CREATED="1501552842059" MODIFIED="1502329609818">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="when to use it" ID="ID_1566883903" CREATED="1501552854332" MODIFIED="1501552860029">
<node ID="ID_1863652638" CREATED="1501553019874" MODIFIED="1502383080463"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      It is typically used when the client is running in a browser using a scripting language such as Javascript. This grant type does not allow the issuance of a refresh token.
    </p>
    <p>
      <b>Single-page apps (or browser-based apps)</b>&#160;run entirely in the browser after loading the Javascript and HTML source code from a web page. Since the entire source is available to the browser, they cannot maintain the confidentiality of a client secret, so the secret is not used for these apps. The flow is exactly the same as the authorization code flow, but at the last step, the authorization code is exchanged for an access token without using the client secret.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="example" ID="ID_673949225" CREATED="1501552867028" MODIFIED="1501552870029">
<node TEXT="Resource Owner: you&#xa;Resource Server: a Facebook server&#xa;Client: a website using AngularJS for example&#xa;Authorization Server: a Facebook server" ID="ID_1927702575" CREATED="1501553045215" MODIFIED="1501553049553"/>
</node>
<node TEXT="format" ID="ID_1400731730" CREATED="1502386862569" MODIFIED="1502386866963">
<node TEXT="response" ID="ID_1880341992" CREATED="1502386868841" MODIFIED="1502386872779">
<node TEXT="HTTP/1.1 302 Found&#xa;Location: https://example-app.com/redirect#access_token=MyMzFjNTk2NTk4ZTYyZGI3&#xa; &amp;state=dkZmYxMzE2&#xa; &amp;token_type=bearer&#xa; &amp;expires_in=86400" ID="ID_1125518073" CREATED="1502386874249" MODIFIED="1502386880170"/>
</node>
</node>
<node TEXT="scenario" FOLDED="true" ID="ID_713666730" CREATED="1501552870748" MODIFIED="1501552876149">
<node FOLDED="true" ID="ID_1780800767" CREATED="1501553098713" MODIFIED="1501553118869"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        The client (AngularJS) wants to obtain information about your Facebook profile.
      </li>
      <li>
        You are redirected by the browser to the authorization server (Facebook).
      </li>
      <li>
        If you authorize access, the authorization server redirects you to the website with the access token in the URI fragment (not sent to the web server). Example of callback: http://example.com/oauthcallback#access_token=MzJmNDc3M2VjMmQzN.
      </li>
      <li>
        This access token can now be retrieved and used by the client (AngularJS) to query the resource server (Facebook). Example of query: https://graph.facebook.com/me?access_token=MzJmNDc3M2VjMmQzN.
      </li>
    </ol>
  </body>
</html>
</richcontent>
<node TEXT="Maybe you wonder how the client can make a call to the Facebook API with Javascript without being blocked because of the Same Origin Policy? Well, this cross-domain request is possible because Facebook authorizes it thanks to a header called Access-Control-Allow-Origin present in the response." ID="ID_1633014674" CREATED="1501553204951" MODIFIED="1501553249116" LINK="https://developer.mozilla.org/en-US/docs/HTTP/Access_control_CORS#The_HTTP_response_headers"/>
</node>
</node>
<node TEXT="pros/cons" FOLDED="true" ID="ID_1725265697" CREATED="1501552876972" MODIFIED="1501552881070">
<node TEXT="This type of authorization should only be used if no other type of authorization is available. Indeed, it is the least secure because the access token is exposed (and therefore vulnerable) on the client side." ID="ID_736422755" CREATED="1501553270880" MODIFIED="1501553283998">
<icon BUILTIN="clanbomber"/>
</node>
</node>
<node TEXT="sequence diagram" ID="ID_90636713" CREATED="1501552882028" MODIFIED="1501552888198">
<node TEXT="" ID="ID_957438128" CREATED="1501553297633" MODIFIED="1501553326083">
<hook URI="images/implicit_flow.png" SIZE="0.9063444" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="vulnerability" FOLDED="true" ID="ID_910107327" CREATED="1501554670527" MODIFIED="1501554745719">
<icon BUILTIN="stop-sign"/>
<node TEXT="This type of authorization is the least secure of all because it exposes the access token to client-side (Javascript most of the time). There is a widespread hole that stems from the fact that the client does not know if the access token was generated for him or not (Confused Deputy Problem)." ID="ID_662464492" CREATED="1501554733715" MODIFIED="1501554738501"/>
<node TEXT="workaround" FOLDED="true" ID="ID_1707509026" CREATED="1501554750076" MODIFIED="1501554830165">
<icon BUILTIN="idea"/>
<node TEXT="To avoid this, the authorization server must provide in its API a way to retrieve access token information. Thus, website A would be able to compare the client_id of the access token of the attacker against its own client_id. As the stolen access token was generated for the website B, client_id would have been different from client_id of website A and the connection would have been refused." ID="ID_42667108" CREATED="1501554848286" MODIFIED="1501554852008"/>
</node>
</node>
</node>
<node TEXT="Resource Owner&#xa;Password Credentials Grant" FOLDED="true" ID="ID_1607478440" CREATED="1501552908325" MODIFIED="1502329609820">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="when to use it" FOLDED="true" ID="ID_998563809" CREATED="1501552854332" MODIFIED="1501552860029">
<node TEXT="With this type of authorization, the credentials (and thus the password) are sent to the client and then to the authorization server. It is therefore imperative that there is absolute trust between these two entities. It is mainly used when the client has been developed by the same authority as the authorization server. For example, we could imagine a website named example.com seeking access to protected resources of its own subdomain api.example.com. The user would not be surprised to type his login/password on the site example.com since his account was created on it." ID="ID_106544811" CREATED="1501553495996" MODIFIED="1501553500182"/>
</node>
<node TEXT="example" FOLDED="true" ID="ID_826267041" CREATED="1501552867028" MODIFIED="1501552870029">
<node TEXT="Resource Owner: you having an account on acme.com website of the Acme company&#xa;Resource Server: Acme company exposing its API at api.acme.com&#xa;Client: acme.com website from Acme company&#xa;Authorization Server: an Acme server" ID="ID_1335759999" CREATED="1501553522213" MODIFIED="1501553526246"/>
</node>
<node TEXT="scenario" FOLDED="true" ID="ID_1678370738" CREATED="1501552870748" MODIFIED="1501552876149">
<node ID="ID_159292286" CREATED="1501553564863" MODIFIED="1501553583260"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        Acme company, doing things well, thought to make available a RESTful API to third-party applications.
      </li>
      <li>
        This company thinks it would be convenient to use its own API to avoid reinventing the wheel.
      </li>
      <li>
        Company needs an access token to call the methods of its own API.
      </li>
      <li>
        For this, company asks you to enter your login credentials via a standard HTML form as you normally would.
      </li>
      <li>
        The server-side application (website acme.com) will exchange your credentials against an access token from the authorization server (if your credentials are valid, of course).
      </li>
      <li>
        This application can now use the access token to query its own resource server (api.acme.com).
      </li>
    </ol>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="pros/cons" ID="ID_83386397" CREATED="1501552876972" MODIFIED="1501552881070"/>
<node TEXT="sequence diagram" FOLDED="true" ID="ID_266129773" CREATED="1501552882028" MODIFIED="1501552888198">
<node TEXT="" ID="ID_1329476372" CREATED="1501553606936" MODIFIED="1501553656751">
<hook URI="images/resource%20owner%20password.png" SIZE="0.8426966" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Client Credentials Grants" FOLDED="true" ID="ID_1671863293" CREATED="1501552981780" MODIFIED="1502329609821">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="when to use it" FOLDED="true" ID="ID_353105808" CREATED="1501552854332" MODIFIED="1501552860029">
<node TEXT="This type of authorization is used when the client is himself the resource owner. There is no authorization to obtain from the end-user.&#xa;For application access." ID="ID_69891517" CREATED="1501553782585" MODIFIED="1502328048640"/>
</node>
<node TEXT="example" FOLDED="true" ID="ID_633200737" CREATED="1501552867028" MODIFIED="1501552870029">
<node TEXT="Resource Owner: any website&#xa;Resource Server: Google Cloud Storage&#xa;Client: the resource owner&#xa;Authorization Server: a Google server" ID="ID_1118378312" CREATED="1501553804722" MODIFIED="1501553808515"/>
</node>
<node TEXT="format" ID="ID_82530710" CREATED="1502387992510" MODIFIED="1502387997424">
<node TEXT="request for access token" ID="ID_546699380" CREATED="1502388023320" MODIFIED="1502388136203">
<node TEXT="POST /token HTTP/1.1&#xa;Host: authorization-server.com&#xa; &#xa;grant_type=client_credentials&#xa;&amp;client_id=xxxxxxxxxx&#xa;&amp;client_secret=xxxxxxxxxx" ID="ID_1281680207" CREATED="1502388050809" MODIFIED="1502388055699"/>
</node>
</node>
<node TEXT="scenario" FOLDED="true" ID="ID_197230226" CREATED="1501552870748" MODIFIED="1501552876149">
<node ID="ID_702758529" CREATED="1501553834898" MODIFIED="1501553848264"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol>
      <li>
        A website stores its files of any kind on Google Cloud Storage.
      </li>
      <li>
        The website must go through the Google API to retrieve or modify files and must authenticate with the authorization server.
      </li>
      <li>
        Once authenticated, the website obtains an access token that can now be used for querying the resource server (Google Cloud Storage).
      </li>
    </ol>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="pros/cons" FOLDED="true" ID="ID_1385098821" CREATED="1501552876972" MODIFIED="1501552881070">
<node TEXT="the end-user does not have to give its authorization for accessing the resource server." ID="ID_1381158761" CREATED="1501553878608" MODIFIED="1501553882409"/>
</node>
<node TEXT="sequence diagram" FOLDED="true" ID="ID_558590095" CREATED="1501552882028" MODIFIED="1501552888198">
<node TEXT="" ID="ID_1364900130" CREATED="1501553887784" MODIFIED="1501553917988">
<hook URI="images/client_credentials_flow.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Refresh Token" FOLDED="true" ID="ID_1584171785" CREATED="1502389556960" MODIFIED="1502389562490">
<node TEXT="when to use it" ID="ID_1023479802" CREATED="1502389611386" MODIFIED="1502389616523">
<node ID="ID_161681522" CREATED="1502389619794" MODIFIED="1502389662121"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If your service issues refresh tokens along with the access token, then you&#8217;ll need to implement the Refresh grant type.
    </p>
    <p>
      <b>The refresh grant can only be used with confidential clients that have a client secret</b>.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="format" ID="ID_997191446" CREATED="1502389739866" MODIFIED="1502389743643">
<node TEXT="POST /oauth/token HTTP/1.1&#xa;Host: authorization-server.com&#xa; &#xa;grant_type=refresh_token&#xa;&amp;refresh_token=xxxxxxxxxxx&#xa;&amp;client_id=xxxxxxxxxx&#xa;&amp;client_secret=xxxxxxxxxx" ID="ID_987216535" CREATED="1502389744858" MODIFIED="1502389749355">
<node ID="ID_1163669872" CREATED="1502389813725" MODIFIED="1502389885504"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>grant_type (required)</b>
    </p>
    <p>
      The grant_type parameter must be set to &#8220;refresh_token&#8221;.
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1789531819" CREATED="1502389836750" MODIFIED="1502389896878"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>refresh_token (required)</b>
    </p>
    <p>
      The refresh token previously issued to the client.
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1895287968" CREATED="1502389859988" MODIFIED="1502389916573"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>scope (optional)</b>
    </p>
    <p>
      The requested scope must not include additional scopes that were not issued in the original access token. <u>Typically this will not be included in the request, and if omitted, the service should issue an access token with the same scope as was previously issued</u>.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Client Authentication (required if the client was issued a secret)" ID="ID_458067179" CREATED="1502389951364" MODIFIED="1502389954022"/>
</node>
</node>
</node>
</node>
<node TEXT="tokens" FOLDED="true" ID="ID_1434963780" CREATED="1501549766958" MODIFIED="1501549785849">
<node TEXT="Tokens are random strings generated by the authorization server and are issued when the client requests them" ID="ID_869104092" CREATED="1501549815223" MODIFIED="1501549820921"/>
<node TEXT="types" ID="ID_131401421" CREATED="1501549832824" MODIFIED="1501549835569">
<node TEXT="Access Token" ID="ID_1366722115" CREATED="1501549838456" MODIFIED="1502383303803">
<node TEXT="this is the most important because it allows the user data from being accessed by a third-party application. This token is sent by the client as a parameter or as a header in the request to the resource server. It has a limited lifetime, which is defined by the authorization server. It must be kept confidential as soon as possible but we will see that this is not always possible, especially when the client is a web browser that sends requests to the resource server via Javascript." ID="ID_174797301" CREATED="1501549950808" MODIFIED="1502383303802"/>
</node>
<node TEXT="Refresh Token" ID="ID_221779696" CREATED="1501549845360" MODIFIED="1501549850242">
<node TEXT="this token is issued with the access token but unlike the latter, it is not sent in each request from the client to the resource server. It merely serves to be sent to the authorization server for renewing the access token when it has expired. For security reasons, it is not always possible to obtain this token." ID="ID_693714163" CREATED="1501549970664" MODIFIED="1501550015147"/>
</node>
<node TEXT="self encoded tokens" ID="ID_1099132127" CREATED="1502389121979" MODIFIED="1502389129428">
<node TEXT="Self-encoded tokens provide a way to avoid storing tokens in a database by encoding all of the necessary information in the token string itself. The main benefit of this is that API servers are able to verify access tokens without doing a database lookup on every API request, making the API much more easily scalable." ID="ID_1230033026" CREATED="1502389131107" MODIFIED="1502389135533">
<node TEXT="A common technique for this is using the JSON Web Signature (JWS) standard to handle encoding, decoding and verification of tokens. The JSON Web Token (JWT) specification defines some terms you can use in the JWS, as well as defines some timestamp terms to determine whether a token is valid" ID="ID_1964592136" CREATED="1502389392188" MODIFIED="1502389396670"/>
</node>
<node ID="ID_1753051292" CREATED="1502389208415" MODIFIED="1502389308479">
<icon BUILTIN="messagebox_warning"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If you <b>already have a distributed database system that is horizontally scalable, then you may not gain any benefits by using self-encoded tokens.</b>&#160;In fact, using <b><font color="#ff0066">self-encoded tokens</font></b>&#160;if you&#8217;ve already solved the distributed database problem <font color="#ff0066"><b>will only introduce new issues, as invalidating self-encoded tokens becomes an additional hurdle</b></font>.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Access Token scope" FOLDED="true" ID="ID_657976932" CREATED="1501550105184" MODIFIED="1501550120371">
<node TEXT="The scope is a parameter used to limit the rights of the access token. Is the authorization server that defines the list of the available scopes. The client must then send the scopes he wants to use for his application during the request to the authorization server." ID="ID_1682121859" CREATED="1501550123242" MODIFIED="1501550146109">
<node TEXT="Read vs Write" ID="ID_315234147" CREATED="1502387166695" MODIFIED="1502387172608"/>
<node TEXT="Restricting access to sensitive data" ID="ID_1735374449" CREATED="1502387179002" MODIFIED="1502387190931"/>
<node TEXT="Selectively Enabling Access by Functionality" FOLDED="true" ID="ID_51961983" CREATED="1502387244956" MODIFIED="1502387251317">
<node TEXT="A great use of scope is to selectively enable access to a user&#x2019;s account based on the functionality needed." ID="ID_406621801" CREATED="1502387279967" MODIFIED="1502387284280"/>
</node>
<node TEXT="limiting access to billable resources" ID="ID_104705743" CREATED="1502387306400" MODIFIED="1502387318641"/>
</node>
</node>
<node TEXT="Making Authenticated request" FOLDED="true" ID="ID_1848623282" CREATED="1501554242862" MODIFIED="1502383362447">
<node TEXT="There are two ways API servers may accept Bearer tokens. One is in the HTTP Authorization header, the other is in a post body parameter. It&#x2019;s up to the service which it supports, so you will need to check the documentation to know for sure." FOLDED="true" ID="ID_606728924" CREATED="1501554268519" MODIFIED="1502383955653">
<node ID="ID_1165421658" CREATED="1501554288735" MODIFIED="1501554356287"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Request parameter (GET or POST)</b>
    </p>
    <p>
      
    </p>
    <p>
      Example using GET: https://api.example.com/profile?access_token=MzJmNDc3M2VjMmQzN
    </p>
    <p>
      <font color="#ff0033"><b>This is not ideal because the token can be found in the access logs of the web server.</b></font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_361241457" CREATED="1501554309216" MODIFIED="1501554393899"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Authorization header</b>
    </p>
    <p>
      
    </p>
    <p>
      GET /profile HTTP/1.1
    </p>
    <p>
      Host: api.example.com
    </p>
    <p>
      Authorization: Bearer MzJmNDc3M2VjMmQzN
    </p>
    <p>
      
    </p>
    <p>
      <font color="#ffcc00"><b>It is elegant but all resource servers do not allow this.</b></font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Verifying Access Tokens" FOLDED="true" ID="ID_1964744016" CREATED="1502390320697" MODIFIED="1502390334258">
<node TEXT="The resource server will be getting requests from applications with an HTTP Authorization header containing an access token. The resource server needs to be able to verify the access token to determine whether to process the request, and find the associated user account, etc." ID="ID_1396016772" CREATED="1502390547782" MODIFIED="1502390552583">
<node TEXT="self-encoded access tokens" FOLDED="true" ID="ID_204696277" CREATED="1502390566654" MODIFIED="1502390577303">
<node TEXT="the verification of the tokens can be done entirely in the resource server without interacting with a database or external servers." ID="ID_1676976536" CREATED="1502390632052" MODIFIED="1502390661659"/>
</node>
<node TEXT="database" FOLDED="true" ID="ID_321603748" CREATED="1502390578479" MODIFIED="1502390582408">
<node TEXT="the verification of the token is a database lookup on the token table." ID="ID_570972366" CREATED="1502390683693" MODIFIED="1502390717541"/>
</node>
<node TEXT="introspection" FOLDED="true" ID="ID_143528195" CREATED="1502390583575" MODIFIED="1502390589640">
<node TEXT="use the Token Introspection spec to build an API to verify access tokens. This is a good way to handle verifying access tokens across a large number of resource servers, since it means you can encapsulate all of the logic of access tokens in a single server, exposing the information via an API to other parts of the system. The token introspection endpoint is intended to be used only internally, so you will want to protect it with some internal authorization, or only enable it on a server within the firewall of the system." ID="ID_277183647" CREATED="1502390766344" MODIFIED="1502390770994"/>
</node>
</node>
<node TEXT="Verifying scope" ID="ID_1224324814" CREATED="1502390978241" MODIFIED="1502390986562">
<node ID="ID_246879210" CREATED="1502391031602" MODIFIED="1502391121042"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <u>The resource server needs to know the list of scopes that are associated with the access token. </u>The server is responsible for denying the request if the scopes in the access token do not include the required scope to perform the designated action.
    </p>
    <p>
      <b>The OAuth 2.0 spec does not define any scopes itself, nor is there a central registry of scopes.</b>&#160;The list of scopes is up to the service to decide for itself.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
