In Studio
1.- {{policyId}}    doesn't trow any value
2.- id="{{policyId}}"  fails because of issue 1.
    <?xml version="1.0" encoding="UTF-8"?>
    <policy
    	xmlns="http://www.mulesoft.org/schema/mule/policy"
    			id="9085029"
    			policyName="jwt-validation-policy"
3.- HTTP config doesn't accepts {{myport}}, only option is to hardcode the value.

In anypoint platform
4.- HTTP config doesn't accepts {{myhost}},{{myport}},{{mypath}}, only option is to hardcode the values.
